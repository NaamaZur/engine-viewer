#include "uiwidget.h"
#include "ui_widget.h"
UIWidget::UIWidget(QWidget *parent) :
    QWidget(parent),
       ui(new Ui::Widget)
{
  ui->setupUi(this);

}
QSize UIWidget::minimumSizeHint() const{

  return QSize(300, 480);

}
QSize UIWidget::sizeHint() const{
  return QSize(300, 480);

}
