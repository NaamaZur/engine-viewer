#ifndef WIDGET_H
#define WIDGET_H


#include "GL/glew.h"
#include "IDMEngine.h"

//#include "enginecontainer.h"
#include <QGLWidget>
#include "utils/FPSCounter.h"

using namespace ie_engine;


class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    explicit GLWidget(QGLFormat &format,QWidget *parent = 0);
    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void keyPressEvent(QKeyEvent *k);

    ~GLWidget();

private:
      ViewSP _view;
    RendererBaseSP _renderer;

     CameraSP _camera;
     std::vector<ContainerSP > _containers;
    int _angleX;
     FPSCounter _fps;
    bool _autoUpdate;
    bool _useDof;
    float _focalDistance;
    float _focusRange;
    float _maxBlurRadius;
    float _aperture;
    float _blurlevel;
    void UpdateDOF();

public slots:
    void SetXRotation(int angle);
    void EnableAutoUpdate();
    void EnableDOF();
    void SetFocalDistance(int value);
    void SetFocusRange(int value);
    void SetMaxBlurRadius(int value);
    void SetAperture(double value);
signals:
    void xRotationChanged(int angle);
    void fpsChanged(double fps);
    void maxBlurRadChanged(double value);
    void focalDistanceChanged(double value);
    void focusRangeChanged(double value);
    void apertureChanged(double value);
};

#endif // WIDGET_H
