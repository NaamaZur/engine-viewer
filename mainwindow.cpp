
#include "mainwindow.h"
#include <QtGui>
#include "QGLFormat"
#include "ui_widget.h"
using namespace Ui;
MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent)

{
    installEventFilter(this);
    setFocusPolicy(Qt::StrongFocus);


    QGLFormat format;
    format.setProfile(QGLFormat::CompatibilityProfile);
    format.setVersion(4,2);
    format.setDoubleBuffer(true);
    format.setSwapInterval(1);

    if(format.hasOpenGL()){
        printf("Has OpenGL");
    }else{
        printf("Has no OpenGL");

    }


    glWidget = new GLWidget(format);
    uiw  = new UIWidget();
    xSlider = createSlider();
    ySlider = createSlider();
    zSlider = createSlider();

    connect(uiw->GetUI()->updateDrawButton,SIGNAL(clicked()),glWidget,SLOT(updateGL()));
    connect(uiw->GetUI()->autoRedrawButton,SIGNAL(clicked()),glWidget,SLOT(EnableAutoUpdate()));
    // enable/disable DOF:
    connect(uiw->GetUI()->enaableDOF_rb,SIGNAL(clicked()),glWidget,SLOT(EnableDOF()));
    // sends max blur slider value to glwidget maxblurradius property:
    connect(uiw->GetUI()->maxBlurRadSlider,SIGNAL(valueChanged(int)),glWidget,SLOT(SetMaxBlurRadius(int)));
    // glwidget maxblurradius property is sent to UI maxblur value label:
    connect(glWidget,SIGNAL(maxBlurRadChanged(double)),uiw->GetUI()->maxBlurRadvaluelabel,SLOT(setNum(double)));

    //UI sends focus range value of focusrange slider to glwidget focusrange property :
    connect(uiw->GetUI()->focalRangeSlider,SIGNAL(valueChanged(int)),glWidget,SLOT(SetFocusRange(int)));
    //glwidget focusrange property is sent to focusrange value label in UI:
    connect(glWidget,SIGNAL(focusRangeChanged(double)),uiw->GetUI()->focusRangeValueLabel,SLOT(setNum(double)));

    //UI sends focal distance value of focal range slider to glwidget focaldistance property:
     connect(uiw->GetUI()->focalDistanceSlider,SIGNAL(valueChanged(int)),glWidget,SLOT(SetFocalDistance(int)));
    // glwidget focaldistance property is sent to focaldistance value label in UI:
    connect(glWidget,SIGNAL(focalDistanceChanged(double)),uiw->GetUI()->focalDistanceValueLabel,SLOT(setNum(double)));
    connect(uiw->GetUI()->apertureSlider,SIGNAL(valueChanged(int)),glWidget,SLOT(SetAperture(double)));

    connect(glWidget,SIGNAL(fpsChanged(double)),uiw->GetUI()->fpsDisplay,SLOT(display(double)));
    connect(xSlider,SIGNAL(valueChanged(int)),glWidget,SLOT(SetXRotation(int)));


    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(glWidget);

    mainLayout->addWidget(xSlider);
    mainLayout->addWidget(ySlider);
    mainLayout->addWidget(zSlider);
    mainLayout->addWidget(uiw);
    setLayout(mainLayout);

    xSlider->setValue(15 * 16);
    ySlider->setValue(345 * 16);
    zSlider->setValue(0 * 16);
    setWindowTitle(tr("SpaceEngine Viewer"));
}
QSlider *MainWindow::createSlider()
{
    QSlider *slider = new QSlider(Qt::Vertical);

    slider->setRange(0, 360 * 16);
    slider->setSingleStep(16);
    slider->setPageStep(15 * 16);
    slider->setTickInterval(15 * 16);
    slider->setTickPosition(QSlider::TicksRight);
    return slider;
}
void MainWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}
