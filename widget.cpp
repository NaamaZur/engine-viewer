
#include "GL/glew.h"
#include "GL/wglew.h"
#include "widget.h"
#include "ui_widget.h"
#include <QKeyEvent>
#include <iostream>
#include "gtx/random.hpp"

//using namespace glm;
GLWidget::GLWidget(QGLFormat &format,QWidget *parent) :
    QGLWidget(format,parent),_angleX(0),_autoUpdate(false),
    _useDof(false),_focalDistance(15.0f),_focusRange(0.3f),
    _maxBlurRadius(64.0f),_aperture(12.0f),_blurlevel(3.0f)
{

    installEventFilter(this);
    setFocusPolicy(Qt::StrongFocus);
}
QSize GLWidget::minimumSizeHint() const{

    return QSize(640, 480);

}
QSize GLWidget::sizeHint() const{
    return QSize(640, 480);

}
void GLWidget::initializeGL(){

    GLenum err = glewInit();
    if (GLEW_OK != err) {
        printf("GLEW error: %s\n", glewGetErrorString(err));
    } else {
        printf("Glew loaded; using version %s\n", glewGetString(GLEW_VERSION));
    }

    //Create view:
    _view = View3D::CreateNullInstance(false);

    int w =minimumSizeHint().width();// this->size().width();
    int h =minimumSizeHint().height();// this->size().height();
    //Create renderer:
    _renderer = RendererBaseSP(new ScreenRenderer(_view,w ,h));
    //Create camera :
    _camera = Camera3D::CreateInstance(Camera3D::IDM_PERSP,38.6464f,w,h,0.1f,10000.0f);
    _view->SetCurrentRenderer(_renderer);
    _view->SetCamera(_camera);
    _view->SetClearColor(0,1,0,1);
    _view->SetStencilTest(false);
    _view->SetEnableAlphaBlend(true);
    _view->SetDepthTest(false);
    _view->SetCullBackFace(false);

    //-----------   create some sprites  -----------------//
    /*
    int i =0;
    for(; i <30 ; ++i){
        float r = (float)rand()/(float)RAND_MAX;
        float g = (float)rand()/(float)RAND_MAX;
        float b = (float)rand()/(float)RAND_MAX;
        MaterialBaseSP mat = MaterialFactory::CreateColorMaterial(r,g,b,1);
        Sprite3DSP spr = GeomFactory::CreateSprite3D("spr",mat,80,80,Sprite3D::IDM_DYNAMIC);

        spr->Transform().TranslateTo(linearRand(vec3(-300.0f,-200.0f,-1200.0f),vec3(300.0f,200.0f,500.0f)));



        _containers.push_back(spr);
    }

    std::sort(_containers.begin(),_containers.end() ,

              [](ContainerSP sprA ,ContainerSP sprB)->bool{

        return sprA->Transform().GetPosition().z < sprB->Transform().GetPosition().z;
    }

    );

    for ( i = 0; i <_containers.size();++i){

        _view->GetScene()->AddChild(_containers[i]);

    }
    */

    MaterialBaseSP mat = MaterialFactory::CreateTextureMaterial("/PO2_demo1.png",Texture::IDM_FILTER_LINEAR,false,false);
    Sprite3DSP spr1 = GeomFactory::CreateSprite3D("spr1",mat,512,512,Sprite3D::IDM_DYNAMIC);
    Sprite3DSP spr2 = GeomFactory::CreateSprite3D("spr2",mat,512,512,Sprite3D::IDM_DYNAMIC);
    Sprite3DSP spr3 = GeomFactory::CreateSprite3D("spr3",mat,512,512,Sprite3D::IDM_DYNAMIC);
    _view->GetScene()->AddChild(spr1);
    spr1->Transform().TranslateTo(-149.0f,240.0f,-1000.0f);
    _view->GetScene()->AddChild(spr2);
    spr2->Transform().TranslateTo(374.0f,240.0f,0.0f);
    _view->GetScene()->AddChild(spr3);
    spr3->Transform().TranslateTo(563.0f,240.0f,400.0f);


_containers.push_back(spr1);
    //  _view->GetScene()->AddChild(spr);

    _camera->SetPosition(320.0f,240.0f,700.0f);


}
void GLWidget::keyPressEvent(QKeyEvent *k){

    switch(k->key()){

    case Qt::Key_Escape:
        exit(EXIT_SUCCESS);
        break;


}


}

void GLWidget::paintGL(){

    _fps.increaseElapsedTime();

    for(size_t i = 0 ; i < _containers.size() ; ++i){
        _containers[i]->Transform().RotateTo(_angleX,-_angleX,_angleX+5);//(linearRand(vec3(0,0,0),vec3(360,360,360)));
    }
    _view->Render();
    //  _framePerSecond = _fps.calulateFramesPerSecondAsFloat();
    double  sfps =(double) _fps.calulateFramesPerSecondAsFloat();
    emit fpsChanged(sfps);
    _fps.increaseNumberFrames();



       // qglColor(Qt::black);
      //  this->renderText(50, 50, "SCISSOR TEST STRING");


    if(_autoUpdate){
        update();
    }

}


void GLWidget::resizeGL(int width, int height){

    glViewport(0,0,width,height);
    paintGL();
}

//////////////////////////////   SLOT METHODS  /////////////////////////////////

//==========================   DOF methods =============================//
void GLWidget::EnableDOF(){
    _useDof =!_useDof;
      _camera->SetDOFisOn(_useDof);

}

void GLWidget::SetFocalDistance(int value){

    _focalDistance =((float)value);
    emit focalDistanceChanged((double)_focalDistance);
    UpdateDOF();
}

void GLWidget::SetFocusRange(int value){
    _focusRange =((float)value /10.0f) ;
    emit focusRangeChanged((double)_focusRange);
    UpdateDOF();
}

void GLWidget::SetMaxBlurRadius(int value){
    _maxBlurRadius =(float)value;
    emit maxBlurRadChanged((double)_maxBlurRadius);
    UpdateDOF();
}
void GLWidget::SetAperture(double value){
    _aperture= (float)value;
    emit apertureChanged((double)_aperture);
    UpdateDOF();
}

void GLWidget::UpdateDOF(){
    if(_useDof){
        _camera->SetDofParams(_maxBlurRadius,_focalDistance,_focusRange,_aperture,100);
    }
}

//======================================================================//

void GLWidget::EnableAutoUpdate(){
    _autoUpdate = !_autoUpdate;
    updateGL();
}

void GLWidget::SetXRotation(int angle){
    _angleX  = angle;
    emit xRotationChanged(angle);
    updateGL();
}

GLWidget::~GLWidget()
{

   _view->Dispose();
}
