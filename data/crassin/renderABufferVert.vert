/**
 * Fast Single-pass A-Buffer using OpenGL 4.0
 * Copyright Cyril Crassin, June 2010
**/

#version 420 core

//uniform mat4 projectionMat;
//uniform mat4 modelViewMat;
//uniform mat4 modelViewMatIT;

//in vec3 vertexPos;
//in vec3 vertexNormal;
uniform mat4 MVP_MATRIX;
uniform mat4 MODEL_VIEW_MATRIX;
uniform mat4 modelViewMatIT;
layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

smooth out vec4 fragPos;
smooth out vec3 fragTexCoord;

smooth out vec3 fragNormal;
out gl_PerVertex
{
    vec4 gl_Position;
};

void main(){
	vec4 pos= MVP_MATRIX *  vec4(position.xyz, 1.0f);

	vec3 normalEye=normalize( (modelViewMatIT * vec4(normal, 1.0f)).xyz );

	fragTexCoord.xy=position.xy;
	fragTexCoord.z=abs(normalEye.z);
	
	fragNormal=normalEye;
	
	fragPos=pos;
	gl_Position = pos;
}