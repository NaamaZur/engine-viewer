/**
 * Fast Single-pass A-Buffer using OpenGL 4.0
 * Copyright Cyril Crassin, June 2010
**/

#version 420 core


//in vec4 vertexPos;
layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;


smooth out vec4 fragPos;
out gl_PerVertex
{
    vec4 gl_Position;
};
void main(){
	fragPos = position;
	gl_Position = position;
}