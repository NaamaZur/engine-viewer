#version 120 

/*
 * OpenGL Programming Guide - Double Write Example
 */


// This is the output color
//layout (location = 0) out vec4 color;

void main(void)
{
     vec2 uv0 = gl_TexCoord[0].st ;
  //  color =  vec4(0,0,1,1);
    gl_FragColor = vec4(uv0.s,uv0.t,0,1);
}