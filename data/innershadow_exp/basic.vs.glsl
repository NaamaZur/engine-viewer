/*******************************************************************************
*******************************************************************************/
#version 420 core

in vec2 aPosition;

noperspective out vec2 vTexcoord;

/*----------------------------------------------------------------------------*/
void main() {
	vTexcoord = aPosition * 0.5 + 0.5;
	gl_Position = vec4(aPosition, 0.0, 1.0);
}