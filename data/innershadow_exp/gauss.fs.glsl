/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform sampler2D uInputTex; // unpremultiplied argb

#define MAX_BLUR_RADIUS 4096

uniform int uBlurriness;
uniform vec2 uBlurDirection; // (1,0)/(0,1) for x/y pass

noperspective in vec2 vTexcoord; // unpremultiplied argb

out float fResult;

/*----------------------------------------------------------------------------*/
/*	Incremental, forward-differencing Gaussian elimination based on:
	http://http.developer.nvidia.com/GPUGems3/gpugems3_ch40.html */
float incrementalGauss1D(
	in sampler2D srcTex, 
	in vec2 srcTexelSize, 
	in vec2 origin,
	in float radius,
	in vec2 direction
) {
	int nSamples = clamp(int(radius), 1, int(MAX_BLUR_RADIUS));
	if (nSamples <= 2)
		return texture(srcTex, origin);
		
	float kSigma		= float(radius) / 3.0;
	float kSigma2		= kSigma * kSigma;
	const float kTau	= 6.2831853071795;
		
//	set up incremental counter:
	vec3 gaussInc;
	gaussInc.x = 1.0 / (sqrt(kTau) * kSigma);
	gaussInc.y = exp(-0.5 / kSigma2);
	gaussInc.z = gaussInc.y * gaussInc.y;
	
//	accumulate results:
	float result = texture(srcTex, origin).r * gaussInc.x;
	for (int i = 1; i < nSamples; ++i) {
		gaussInc.xy *= gaussInc.yz;
		
		vec2 offset = float(i) * direction * srcTexelSize;
		result += texture(srcTex, origin - offset).r * gaussInc.x;
		result += texture(srcTex, origin + offset).r * gaussInc.x;
	}
	
	return result;
}

/*----------------------------------------------------------------------------*/
void main() {
	fResult = incrementalGauss1D(
		uInputTex, 
		1.0 / vec2(textureSize(uInputTex, 0)), 
		vTexcoord, 
		float(uBlurriness), 
		uBlurDirection
	);
}