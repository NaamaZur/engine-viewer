/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform sampler2D uDistanceField;
uniform float uSize;
uniform float uSpread;

noperspective in vec2 vTexcoord;

out vec4 fResult;

const float kTexLevels = 65536.0;
const float kZero = 0.5 * kTexLevels / (kTexLevels - 1.0);
const float kInfinity = 0.0;

/*----------------------------------------------------------------------------*/
vec2 map(vec2 x) {
	return x * (kTexLevels - 1.0) / kTexLevels * 2.0 - 1.0;
}

/*----------------------------------------------------------------------------*/
void main() {		
	vec4 dvec = texture(uDistanceField, vTexcoord);
	dvec.zw = map(dvec.zw);

	float d = clamp((length(dvec.zw) - uSize) / (uSize * uSpread - uSize), 0.0, 1.0);
	float fac = smoothstep(1.0, 0.0, d);
	fResult = vec4(fac);
}