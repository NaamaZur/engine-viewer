#version 430 core

subroutine vec4 BlendMode(vec4 base,vec4 blend);
subroutine uniform BlendMode blendTechnique;

#define         IDM_NORMAL_BLEND           0 
#define         IDM_ADD_BLEND              1
#define	        IDM_OVERLAY_BLEND          2
#define		IDM_SUBTRACT_BLEND         3  
#define		IDM_LIGHTEN_BLEND          4
#define		IDM_DARKEN_BLEND           5
#define		IDM_SCREEN_BLEND           6
#define		IDM_MULTIPLY_BLEND         7
#define		IDM_DIFFERENCE_BLEND       8
#define		IDM_COLOR_DODGE_BLEND      9
#define		IDM_COLOR_BURN_BLEND       10
#define		IDM_EXCLUSION_BLEND        11
#define		IDM_LINEAR_LIGHT_BLEND     12
#define		IDM_VIVID_LIGHT_BLEND      13
#define		IDM_HARD_MIX_BLEND         14
#define		IDM_REFLECT_BLEND          15
#define		IDM_GLOW_BLEND             16
#define		IDM_PIN_LIGHT_BLEND        17

layout(binding=0) uniform sampler2D baseSample;
layout(binding=1) uniform sampler2D blendSample;
layout(binding=2) uniform sampler2D TRACK_MATT_MAP;

#define IDM_MASK_NONE      0
#define IDM_ALPHA          1
#define IDM_LUMA           2
#define IDM_ALPHA_INVRT    3
#define IDM_LUMA_INVRT     4

uniform int TRACKMATT_ACTIVE = 0;
uniform int blendMode= 0;
uniform int forcePremult = 0 ;


//in vec2 uvsOut;
out vec4 outputColor;


///**************  BLEND MODES  ******************////
#define Blend(base, blend, funcf) 		vec3(funcf(base.r, blend.r), funcf(base.g, blend.g), funcf(base.b, blend.b))
#define BlendLinearDodgef 			    BlendAddf
#define BlendLinearBurnf 			    BlendSubstractf
#define BlendAddf(base, blend) 		    min(base + blend, 1.0)
#define BlendSubstractf(base, blend) 	max(base + blend - 1.0, 0.0)
#define BlendLightenf(base, blend) 		max(blend, base)
#define BlendDarkenf(base, blend) 		min(blend, base)
#define BlendLinearLightf(base, blend) 	(blend < 0.5 ? BlendLinearBurnf(base, (2.0 * blend)) : BlendLinearDodgef(base, (2.0 * (blend - 0.5))))
#define BlendScreenf(base, blend) 		(1.0 - ((1.0 - base) * (1.0 - blend)))
#define BlendOverlayf(base, blend) 	    (base < 0.5 ? (2.0 * base * blend) : (1.0 - 2.0 * (1.0 - base) * (1.0 - blend)))
#define BlendSoftLightf(base, blend) 	((blend < 0.5) ? (2.0 * base * blend + base * base * (1.0 - 2.0 * blend)) : (sqrt(base) * (2.0 * blend - 1.0) + 2.0 * base * (1.0 - blend)))
#define BlendColorDodgef(base, blend) 	((blend == 1.0) ? blend : min(base / (1.0 - blend), 1.0))
#define BlendColorBurnf(base, blend) 	((blend == 0.0) ? blend : max((1.0 - ((1.0 - base) / blend)), 0.0))
#define BlendVividLightf(base, blend) 	((blend < 0.5) ? BlendColorBurnf(base, (2.0 * blend)) : BlendColorDodgef(base, (2.0 * (blend - 0.5))))
#define BlendPinLightf(base, blend) 	((blend < 0.5) ? BlendDarkenf(base, (2.0 * blend)) : BlendLightenf(base, (2.0 *(blend - 0.5))))
#define BlendHardMixf(base, blend) 	    ((BlendVividLightf(base, blend) < 0.5) ? 0.0 : 1.0)
#define BlendReflectf(base, blend) 		((blend == 1.0) ? blend : min(base * base / (1.0 - blend), 1.0))


//*************  NORMAL BLEND ********************///

layout(index = IDM_NORMAL_BLEND)subroutine( BlendMode )
 vec4 BlendNormal(vec4 base,vec4 blend){
	  return blend;
 }
 //*************  ADDITIVE BLEND ********************///
 layout(index = IDM_ADD_BLEND)subroutine( BlendMode )
   vec4 Add(vec4 base,vec4 blend){
   return min(base + blend, vec4(1.0));
  }
 //***************************   OVERLAY BLEND *************************** /////
 layout(index = IDM_OVERLAY_BLEND)subroutine( BlendMode )
   vec4  BlendOverlay(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendOverlayf),blend.a);
 } 
//************* SUBTRACT BLEND ********************///
layout(index = IDM_SUBTRACT_BLEND) subroutine( BlendMode )
 vec4 BlendSubstract(vec4 base,vec4 blend){
   return  max(base + blend - vec4(1.0), vec4(0.0));
 }
//*************  LIGHTEN BLEND ********************///
 layout(index = IDM_LIGHTEN_BLEND) subroutine( BlendMode )
   vec4 BlendLighten(vec4 base,vec4 blend){
	  return vec4(max(blend,base));
   }
   //*************  DARKEN BLEND ********************///
layout(index = IDM_DARKEN_BLEND)  subroutine( BlendMode )
   vec4 BlendDarken(vec4 base,vec4 blend){
	  return vec4(min(blend,base));
   }
  //*************  SCREEN BLEND ********************///
layout(index = IDM_SCREEN_BLEND) subroutine( BlendMode )
   vec4  BlendScreen(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendScreenf),blend.a);
   } 
   //*************  MULTIPLY BLEND ********************///
layout(index = IDM_MULTIPLY_BLEND) subroutine( BlendMode )
   vec4  BlendMultiply(vec4 base,vec4 blend){
	  return (blend * base);
   }
   //*************  DIFFERENCE BLEND ********************/// 
 layout(index = IDM_DIFFERENCE_BLEND) subroutine( BlendMode )
   vec4  BlendDifference(vec4 base,vec4 blend){
	  return (abs(base - blend));
   }
  //*******************   COLOR DODGE BLEND  ***************************** ////
 layout(index = IDM_COLOR_DODGE_BLEND) subroutine( BlendMode )
   vec4  BlendColorDodge(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendColorDodgef),blend.a);
   }
   //*******************   COLOR BURN BLEND  ***************************** ////
 layout(index = IDM_COLOR_BURN_BLEND) subroutine( BlendMode )
   vec4  BlendColorBurn(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendColorBurnf),blend.a);
   }
   //*******************   EXCLUSION BLEND  ***************************** ////
layout(index = IDM_EXCLUSION_BLEND) subroutine( BlendMode )
   vec4  BlendExclusion(vec4 base,vec4 blend){
	  return (base + blend - 2.0 * base * blend);
   }
   
  //*******************  LINEAR LIGHT BLEND  ***************************** ////
 layout(index = IDM_LINEAR_LIGHT_BLEND) subroutine( BlendMode )
   vec4  BlendLinearLight(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendLinearLightf),blend.a);
   }
	
   //*******************   VIVID LIGHT BLEND  ***************************** ////
layout(index = IDM_VIVID_LIGHT_BLEND) subroutine( BlendMode )
   vec4  BlendVividLight(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendVividLightf),blend.a);
   }
	 
   //*******************   HARD MIX BLEND  ***************************** ////
layout(index = IDM_HARD_MIX_BLEND) subroutine( BlendMode )
   vec4  BlendHardMix(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendHardMixf),blend.a);
   }
 

	//*******************   REFLECT BLEND  ***************************** ////
layout(index = IDM_REFLECT_BLEND)subroutine( BlendMode )
   vec4  BlendReflect(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendReflectf),blend.a);
   }  
	   
	//*******************   GLOW BLEND  ***************************** ////
layout(index = IDM_GLOW_BLEND)subroutine( BlendMode )
	vec4  BlendGlow(vec4 base,vec4 blend){
	  return BlendReflect(blend, base);
	} 
	//*******************   PIN LIGHT BLEND *************************** //////
layout(index = IDM_PIN_LIGHT_BLEND)subroutine( BlendMode )
	vec4  BlendPinLight(vec4 base,vec4 blend){
	  return vec4(Blend(base, blend, BlendPinLightf),blend.a);
	} 
  
	   
	   

void main()
{ 
	

		   ivec2 tsize = textureSize(baseSample, 0); 
		   vec2 texcoord = gl_FragCoord.xy * (1.0 / vec2(tsize));
		   vec4 baseTex=  texture(baseSample,texcoord);
		   vec4 blendTex= texture(blendSample,texcoord);
  
		  if(forcePremult == 1){
			 blendTex =  vec4(blendTex.rgb * blendTex.a,blendTex.a);
		  }
		 
	 
		  blendTex.rgb/=blendTex.a;  //MUST UNPREMULT BECAUSE THE INPUT TEX IS PREMULT!!!
		  vec4 blendedRes  =   blendTechnique( baseTex , blendTex) ;
	 

	
	vec4 alpha;
	float res = 1.0;
	vec2 texCoordInverted = vec2(texcoord.x,1 - texcoord.y);  ///must flip along Y axis as defaul sampling is inverted
	switch(TRACKMATT_ACTIVE){
	   case IDM_ALPHA: // do alpha  mask //

		 alpha = texture(TRACK_MATT_MAP ,texCoordInverted);
		 res = alpha.a;

	   break;
	   case IDM_LUMA:// do luma mask //

		  alpha = texture(TRACK_MATT_MAP ,texCoordInverted);
		  alpha = vec4(alpha.rgb * alpha.a ,alpha.a);
		  res = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
		   
	   break;
	   case IDM_ALPHA_INVRT:// do inverted alpha mask //

		  alpha = texture(TRACK_MATT_MAP ,texCoordInverted);
		
		  res =  1 - alpha.a;

	   break;
	   case  IDM_LUMA_INVRT:

		  alpha =  texture(TRACK_MATT_MAP ,texCoordInverted);
		  alpha = vec4(alpha.rgb * alpha.a ,alpha.a);
		  res   = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
		  res   = 1 - res;
	   break;
	}
		
	 
	  outputColor=vec4( blendedRes.rgb  * blendTex.a * res , blendTex.a * res);
 
   

}