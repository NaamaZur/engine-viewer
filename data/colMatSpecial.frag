/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Fragment Shader.
*/
#version 420 

smooth in vec2 uvsOut;
flat in vec4 diffuseOut;
out vec4 OUTPUT;  ////RGB only 


void main(void) {
	

	OUTPUT = diffuseOut * uvsOut.x;
	
}