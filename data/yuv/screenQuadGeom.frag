/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Texture pass through Material Fragment Shader.
*/
#version 420 core

layout(binding=0) uniform sampler2D   COLOR_MAP_0;

in vec2 texcoord;
flat in int invID;

out vec4 OUTPUT;
 
void main(void) {

 

	 ///////////////////////////////////////////////////////////////////////
	 ///////////////////////////////////////////////////////////////////////
	  vec3 tc = texture(COLOR_MAP_0, texcoord).rgb;
	  OUTPUT.rgba = vec4(1.0);
	  OUTPUT.a =  texture(COLOR_MAP_0, texcoord).a;
	 switch(invID){

	 case 0: ///viewport 1 (y)
	
 
	   OUTPUT.r = (tc.r * 0.257) + (tc.g * 0.504) + (tc.b * 0.098) + 0.0625;

	 break;
	 case 1://viewport 2   (u)

	   OUTPUT.r = -(tc.r * 0.148) - (tc.g * 0.291) + (tc.b * 0.439) + 0.5;

	 break;
	 case 2://viewport 3  (v)

	    OUTPUT.r =  (tc.r * 0.439) - (tc.g * 0.368) - (tc.b * 0.071) + 0.5;

	 break;
	 default:
	  //	OUTPUT =   texture(COLOR_MAP_0,texcoord) ;
	 break;
	 }
	 
	// 	OUTPUT =   texture(COLOR_MAP_0,texcoord) *  vec4(1,0,1,1); 
	 

}