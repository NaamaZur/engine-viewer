 /*******   U encoder  ************/
 #version 420 core
uniform sampler2D u_tex;
in vec2 v_tex;
out vec4 v_channel;
void main() {

	//ivec2 tsize = textureSize(u_tex, 0);
	//vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));

	vec3 tc = texture(u_tex, v_tex).rgb;
	v_channel = vec4(1.0);
	v_channel.r =  (tc.r * 0.439) - (tc.g * 0.368) - (tc.b * 0.071) + 0.5;
}
