 /*******   U encoder  ************/
 #version 420 core
  layout(binding = 0)uniform sampler2D u_tex;
in vec2 v_tex;
out vec4 u_channel;
void main() {
	//ivec2 tsize = textureSize(u_tex, 0);
//	vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));
	vec3 tc = texture(u_tex, v_tex).rgb;
	u_channel = vec4(1.0);
	u_channel.r = -(tc.r * 0.148) - (tc.g * 0.291) + (tc.b * 0.439) + 0.5;
}
