   /*******   Y encoder  ************/
#version 420 core
layout(binding = 0)uniform sampler2D u_tex;
in vec2 v_tex;
out vec4 fragcol;
void main() {
//	ivec2 tsize = textureSize(u_tex, 0);
//	vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));
	vec3 tc = texture(u_tex, v_tex).rgb;
	fragcol.rgba = vec4(1.0);
	// we need to set all channels
	fragcol.r = (tc.r * 0.257) + (tc.g * 0.504) + (tc.b * 0.098) + 0.0625;
}
