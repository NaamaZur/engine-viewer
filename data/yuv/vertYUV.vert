	#version  420 core

const vec2 vert_data[4] = vec2[](
			   vec2(-1.0, 1.0), 
			   vec2(-1.0, -1.0), 
			   vec2(1.0, 1.0), 
			   vec2(1.0, -1.0) 
			 );

const vec2 tex_data[4] = vec2[]( 
			   vec2(0.0, 1.0), 
			   vec2(0.0, 0.0), 
			   vec2(1.0, 1.0), 
			   vec2(1.0, 0.0)  
			 );
out gl_PerVertex 
{
	vec4 gl_Position;
}
;
out vec2 v_tex;
void main() {
	gl_Position = vec4(vert_data[gl_VertexID], 0.0, 1.0);
	v_tex = tex_data[gl_VertexID];
}
 