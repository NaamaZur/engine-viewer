#version 420 core

layout(triangles , invocations = 3) in;///,invocations = 3
layout(triangle_strip, max_vertices =4) out;

out vec2 texcoord;
flat out int invID;
 
out gl_PerVertex
{
  vec4 gl_Position;
  //float gl_PointSize;
 // float gl_ClipDistance[];
};
void main() 
{
const vec2 vert_data[4] = vec2[](
			   vec2(-1.0, 1.0), 
			   vec2(-1.0, -1.0), 
			   vec2(1.0, 1.0), 
			   vec2(1.0, -1.0) 
			 );


			 const vec2 tex_data[4] = vec2[]( 
			   vec2(0.0, 1.0), 
			   vec2(0.0, 0.0), 
			   vec2(1.0, 1.0), 
			   vec2(1.0, 0.0)  
			 );

	 	 invID = gl_InvocationID;
		
/*
	gl_Position = vec4( -1.0, 1.0, 0, 1.0 );
  //  texcoord = vec2( 1.0, 1.0 );
	EmitVertex();

	gl_Position = vec4(-1.0,-1.0, 0, 1.0 );
//    texcoord = vec2( 0.0, 1.0 ); 
	//EmitVertex();

	gl_Position = vec4( 1.0,1.0, 0, 1.0 );
 //  texcoord = vec2( 1.0, 0.0 ); 
	EmitVertex();

	gl_Position = vec4(1.0,-1.0, 0, 1.0 );
//    texcoord = vec2( 0.0, 0.0 ); 
	EmitVertex();
	*/
   
	for(int i=0; i<4; i++)
  {
	gl_Position =   vec4( vert_data[i].xy,0,1);///gl_Position;
	texcoord = tex_data[i];
	gl_ViewportIndex = gl_InvocationID;
	EmitVertex();
  }
   
  
/*
	gl_Position = vec4( -1.0, 1.0, 0.0, 1.0 );
	texcoord = vec2( 0.0, 1.0 );
	EmitVertex();

	gl_Position = vec4(-1.0, -1.0, 0.0, 1.0 );
	texcoord = vec2( 0.0, 0.0 ); 
	EmitVertex();

	gl_Position = vec4( 1.0, 1.0, 0.0, 1.0 );
	texcoord = vec2( 1.0, 1.0 ); 
	EmitVertex();

	gl_Position = vec4(1.0,-1.0, 0.0, 1.0 );
	texcoord = vec2( 1.0, 0.0 ); 
	EmitVertex();


	*/
	 // gl_InvocationID;
	 EndPrimitive(); 

}