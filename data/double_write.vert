#version 420 core

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP_MATRIX;
uniform mat4 MODEL_VIEW_MATRIX;
uniform mat4 VIEW_MATRIX;
uniform mat4 WORLD_MATRIX;
uniform mat3 NORMAL_MATRIX;
//uniform mat4 projection_matrix;

//uniform float aspect;
//uniform float time;

out gl_PerVertex
{
    vec4 gl_Position;
};

out vec4 surface_color;
out vec3 frag_position;
out vec3 frag_normal;

void main(void)
{
    vec4 offset = vec4(float(gl_InstanceID & 3) * 2.0,
                       float((gl_InstanceID >> 2) & 3) * 2.0,
                       float((gl_InstanceID >> 4) & 3) * 2.0, 0.0) -
                  vec4(4.0, 4.0, 4.0, 0.0);

    surface_color = normalize(offset) * 0.5 + vec4(3.9, 0.1, 0.1, 0.9);

    vec4 object_pos = (position + offset);
    vec4 world_pos = WORLD_MATRIX * object_pos;
    frag_position = world_pos.xyz;
    frag_normal = NORMAL_MATRIX * normal;

    gl_Position = MVP_MATRIX * position ;
}
