#version 420 core

layout(binding=0) uniform sampler2D COLOR_MAP_0;

uniform float MAX_BLUR_RADIUS; 
uniform vec2 BLUR_DIRECTION; // (1,0) or (0,1)

smooth in float BLUR_RADIUS; // from the vertex shader

layout(location=0) out vec4 OUTPUT;

/*---------------------------------------------------------------------------*/
vec4 box1d(
	in sampler2D srcTex, 
	in vec2 srcTexelSize, 
	in vec2 origin,
	in float radius,
	in vec2 direction
) {
	int nSamples = clamp(int(radius) / 2, 1, int(MAX_BLUR_RADIUS)); 
 
	if (nSamples <= 1){
		return texture(srcTex, origin);
	}

//	accumulate results:
	vec4 result = texture(srcTex, origin);
	for (int i = 1; i < nSamples; ++i) {		
		vec2 offset = float(i) * direction * srcTexelSize;
		result += texture(srcTex, origin - offset);
		result += texture(srcTex, origin + offset);
	}
	
	return result / float(nSamples * 2 - 1);
}
/*----------------------------------------------------------------------------*/
vec4 incrementalGauss1D(
	in sampler2D srcTex, 
	in vec2 srcTexelSize, 
	in vec2 origin,
	in float radius,
	in vec2 direction
) {
	int nSamples = clamp(int(radius), 1, int(MAX_BLUR_RADIUS));
	if (nSamples <= 2)
		return texture(srcTex, origin);
	
	float kSigma		= float(nSamples) / 4.0;
	float kSigma2		= kSigma * kSigma;
	const float kTwoPi	= 6.2831853071795;
		
//	set up incremental counter:
	vec3 gaussInc;
	gaussInc.x = 1.0 / (sqrt(kTwoPi) * kSigma);
	gaussInc.y = exp(-0.5 / kSigma2);
	gaussInc.z = gaussInc.y * gaussInc.y;
	
//	accumulate results:
	vec4 result = texture(srcTex, origin) * gaussInc.x;	
	for (int i = 1; i < nSamples; ++i) {
		gaussInc.xy *= gaussInc.yz;
		
		vec2 offset = float(i) * direction * srcTexelSize;
		result += texture(srcTex, origin - offset) * gaussInc.x;
		result += texture(srcTex, origin + offset) * gaussInc.x;
	}
	
	return result;
}

/*----------------------------------------------------------------------------*/
void main() {
	vec2 texelSize = 1.0 / vec2(textureSize(COLOR_MAP_0, 0));
	vec2 screenTexCoords = gl_FragCoord.xy * texelSize;

	/*
	OUTPUT = incrementalGauss1D(
		COLOR_MAP_0, 
		texelSize, 
		screenTexCoords, 
		BLUR_RADIUS, 
		BLUR_DIRECTION
	); 

	*/
	OUTPUT = box1d(
		COLOR_MAP_0, 
		texelSize, 
		screenTexCoords, 
		BLUR_RADIUS, 
		BLUR_DIRECTION
	) ;
}