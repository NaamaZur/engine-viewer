/*	Renders plane outputing blur radius as a interpolant. */

#version 420 core

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP_MATRIX;

/*	MAX_BLUR_RADIUS is used to clamp the final BLUR_RADIUS. */
uniform float MAX_BLUR_RADIUS;

/*	FOCAL_DISTANCE is the depth of the focal plane. */
uniform float FOCAL_DISTANCE;

/*	FOCAL_RANGE is the falloff to max blurriness either side of the focal plane. */
//uniform float FOCAL_RANGE;

uniform float APERTURE; //aperture param as in AE.
uniform float FOCAL_LENGTH;// camera focal lenfth
uniform float BLUR_LEVEL;//in AE this is a % ,must be divided by 100

/*	1/render target dimensions. */
uniform vec2 TEXEL_SIZE;

/*	CLIP_BOUNDS is a scale factor used for creating a screen-aligned bounding
	quad (see getClipSpaceBounds() in the main.cpp) */
uniform vec2 CLIP_BOUNDS;

uniform vec4 uclipPosDelta;

smooth out float BLUR_RADIUS; // circle of confusion radius

out gl_PerVertex
{
	vec4 gl_Position;
};


/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
void main(void) {
//	calculate initial position (for BLUR_RADIUS calculation):
	gl_Position=  MVP_MATRIX * position  ;//// 
	
//	calculate blur radius:
	//============ closed for test:
//	BLUR_RADIUS = abs(gl_Position.z - FOCAL_DISTANCE ) / FOCAL_RANGE;
//	BLUR_RADIUS =clamp(BLUR_RADIUS, 0.0, MAX_BLUR_RADIUS);

///  new try  ////

   BLUR_RADIUS = abs(APERTURE * (FOCAL_LENGTH * (gl_Position.z- FOCAL_DISTANCE)) / (gl_Position.z* (FOCAL_DISTANCE - FOCAL_LENGTH ))) * BLUR_LEVEL;
   BLUR_RADIUS = clamp(BLUR_RADIUS, 0.0, MAX_BLUR_RADIUS); // this is unchanged

   //==================================

	//	construct screen-aligned bounding quad + blur radius;
		gl_Position = MVP_MATRIX[3];                                                                              // clip space origin
		gl_Position.xy += position.xy * CLIP_BOUNDS ;// - vec2(uclipPosDelta.x * 0.5,uclipPosDelta.y * 0.5) ;     //commented block is alternative... // fit around plane bounds
		gl_Position.xy +=  (position.xy * BLUR_RADIUS * TEXEL_SIZE) * gl_Position.w;                              // fit around blur radius
	 
}