/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP_MATRIX;

uniform vec4 DIFFUSE_COLOR;

//=======  OUTS  ============//
out  flat vec4 diffuseOut;

out gl_PerVertex
{
	vec4 gl_Position;

};

void main(void){
   diffuseOut = DIFFUSE_COLOR;
  vec4 pos = MVP_MATRIX *  position;

  gl_Position = pos;

}