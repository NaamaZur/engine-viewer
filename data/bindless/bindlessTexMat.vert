/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 compatibility

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP_MATRIX;

//=======  OUTS  ============//

out smooth  vec2 vUVs;
 


out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	

		 vUVs = uvs;

		 gl_Position = MVP_MATRIX * position;
	 
	
}