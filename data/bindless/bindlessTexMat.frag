#version 420 compatibility
#extension GL_NV_bindless_texture : require
#extension GL_NV_gpu_shader5 : require // for uint64_t

#define NUM_TEXTURES  4

layout(binding=0)uniform SamplersNV ///std140
{ 

  sampler2D allTheSamplers[NUM_TEXTURES];

};
layout(binding=0)uniform sampler2D Diffuse;
in smooth  vec2 vUVs;

out vec4 OUTPUT;

void main (void){

   
   sampler2D s1 = allTheSamplers[0];
   sampler2D s2 = allTheSamplers[1];
   sampler2D s3 = allTheSamplers[2];
   sampler2D s4 = allTheSamplers[3];

   if(vUVs.x < 0.5 && vUVs.y < 0.5){

	  OUTPUT = texture(s1 , vUVs );

   }else if(vUVs.x > 0.5 && vUVs.y < 0.5){

	  OUTPUT = texture(s2 , vUVs );

   }else if(vUVs.x < 0.5 && vUVs.y  > 0.5){

	  OUTPUT = texture(s3 , vUVs );

  }else{

	  OUTPUT = texture(s4 , vUVs );

  }

	


}


