/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Fragment Shader.
*/
#version 420 

/*
in DataOut{

smooth  vec2 uvsOut;
flat    vec4 diffuseOut;

}VertexIn;
*/

smooth in vec2 uvsOut;
flat in vec4 diffuseOut;
in  vec3 Position;
in vec3 Normal;
out vec4 OUTPUT;  ////RGB only 


void main(void) {
	
	
	OUTPUT =  diffuseOut ;//* uvsOut.x /2;/// vec4(uvsOut.x,0,0,1);
	
}