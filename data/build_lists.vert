#version 420 core

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP_MATRIX;
uniform mat4 MODEL_VIEW_MATRIX;
uniform mat4 VIEW_MATRIX;
uniform mat4 WORLD_MATRIX;
uniform mat3 NORMAL_MATRIX;
uniform float OPACITY;
uniform vec4 DIFFUSE_COLOR;
//uniform float aspect;
//uniform float time;

out gl_PerVertex
{
    vec4 gl_Position;
};

out vec4 surface_color;
out vec3 frag_position;
out vec3 frag_normal;

void main(void)
{
    vec4 offset = vec4(float(gl_InstanceID & 7) * 2.0,
                       float((gl_InstanceID >> 3) & 7) * 2.0,
                       float((gl_InstanceID >> 6) & 7) * 2.0, 0.0) -
                  vec4(8.0, 8.0, 8.0, 0.0);

    surface_color =vec4(DIFFUSE_COLOR.rgb,OPACITY);// normalize(offset) * 0.5 + vec4(0.5, 0.5, 0.5, 0.4);

    vec4 object_pos = (position + offset);
    vec4 world_pos = WORLD_MATRIX * object_pos;
    frag_position = world_pos.xyz;
    frag_normal = mat3(WORLD_MATRIX * VIEW_MATRIX) * normal;

    gl_Position = MVP_MATRIX * position;  //(projection_matrix * view_matrix) * world_pos;
}
