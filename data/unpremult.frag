/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Texture pass through Material Fragment Shader.
*/
#version 420 

layout(binding=0) uniform sampler2D  COLOR_MAP_0;


noperspective  in vec2 uvsOut;
out vec4 OUTPUT;
 

void main(void) {

		ivec2 tsize = textureSize(COLOR_MAP_0, 0);
		vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));
		vec4 tex = texture(COLOR_MAP_0,vec2( screntc.x, 1.0 - screntc.y));
		OUTPUT =  tex;// vec4(tex.rgb/tex.a,tex.a);

}