/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP_MATRIX;
uniform vec4 DIFFUSE_COLOR;

smooth out vec2 uvsOut;
flat out vec4 diffuseOut;
out gl_PerVertex
{
    vec4 gl_Position;
};
void main()
{
	

         uvsOut=uvs;
         diffuseOut  =  DIFFUSE_COLOR;
	     gl_Position =  MVP_MATRIX * position;
	
}