/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Texture pass through Material Fragment Shader.
*/
#version 420 

layout(binding=0) uniform sampler2D   COLOR_MAP_0;

out vec4 OUTPUT;

/*
float LinearizeDepth(vec2 uv)
{
  float n = 1.0; // camera z near
  float f = 10000.0; // camera z far
  float z = texture(COLOR_MAP_0, uv).x;
  return (2.0 * n) / (f + n - z * (f - n));	
}
*/

void main(void) {

	ivec2 tsize = textureSize(COLOR_MAP_0, 0);
	vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));

		/*
	  float d;
	  if (screntc.x < 0.5) 
	  {
		d = LinearizeDepth(screntc);
	   } else{

			d = texture2D(COLOR_MAP_0, screntc).r;///texture2D(depthSampler, uv).x;
	   }
	   
	   OUTPUT = vec4(d,d,d,1.0); 
	   */

 
	 
		OUTPUT =  texture(COLOR_MAP_0,screntc);/// vec2(screntc.x,1 - screntc.y));

}