/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 core


//===================  Subroutines ==============================
subroutine vec4 RenderMode();
subroutine uniform RenderMode renderMode;

//============================  UNIFORMS  ==================================//
struct LightInfo{

 vec4 Lp;///light position
 vec3 Li;///light intensity
 vec3 Lc;///light color
 int  Lt;///light type
 vec3 Ld;///light direction (for dir and spot lights only )
 float Ca;///cone angle (spot light only)
 float Cf;///cone Feather (spot light only)

};


layout(binding=0)  uniform sampler2D COLOR_MAP_0;
layout(binding=1)  uniform sampler2D ALPHA_MAP_0;

const int MAX_LIGHTS=15;
uniform LightInfo lights[MAX_LIGHTS];

// material props:
uniform float OPACITY;
uniform vec3 KD;
uniform vec3 KA;
uniform vec3 KS;
uniform float SHININESS;
uniform int num_lights;
uniform vec4 CAM_POS;
uniform int ALPHA_ACTIVE;
//===== in ======================================================
 in smooth vec2 uvsOut;
 in  vec3 Position;
 in smooth vec3 Normal;

 //===== out =====================================================
layout(location = 0)out vec4 OUTPUT;

//=============   DIRECTIONAL LIGHT  ===========================//
vec3 dirlightType( int lightIndex,vec3 position , vec3 normal){

     vec3 n =normalize(normal);
     vec4 lMVPos=lights[lightIndex].Lp ;
     vec3 s= normalize(vec3(lMVPos.xyz)-position); //surf to light
     vec3 v= normalize( - position  ); //   -position eye direction camera space
    
     vec3 h =  normalize(v+s);

     vec3 dir = normalize( lights[lightIndex].Ld - lMVPos.xyz );
   


        float sDotN= max( 0.0 , dot(-dir, n) );
        vec3 diff=    KD  *  lights[lightIndex].Lc * sDotN ;
        diff=clamp(diff ,0.0 ,1.0);
        vec3 spec =vec3(0,0,0);

        if(sDotN >0.0){
           spec =  KS * pow( max( 0.0 ,dot(h,n) ) ,  SHININESS) ;
           spec=clamp(spec ,0.0 ,1.0);
        }

    return lights[lightIndex].Li *  (diff +spec );
}

 
//=================  POINT LIGHT  ============================//

vec3 pointlightType( int lightIndex,vec3 position , vec3 normal){

     vec3 n =  normalize(normal);
     vec4 lMVPos=lights[lightIndex].Lp ;
     vec3 s=   normalize(vec3(lMVPos.xyz) - position); //surf to light
     vec3 v=   normalize( -position  ); //
     vec3 h =  normalize(v+s);
	
      if(!gl_FrontFacing)// if (dot(n, v) < 0.0)  ///take care of backface lightning
     {
      n = -n;

	 }
    
     float sDotN= max(  dot(s, n) ,0.0  );
     vec3 diff=    lights[lightIndex].Lc * KD  *  sDotN ;
     diff=clamp(diff ,0.0 ,1.0);
     vec3 spec =vec3(0,0,0);
     if(sDotN >0.0){
           spec =  lights[lightIndex].Lc * KS *  pow( max( 0.0 ,dot(h,n) ) , SHININESS  ) ;//
           spec=clamp(spec ,0.0 ,1.0);
     }

     return    lights[lightIndex].Li  * ( spec+diff);	
}

//================= SPOT LIGHT   ============================//


vec3 spotlightType( int lightIndex,vec3 position , vec3 normal){

     vec3 n =normalize(normal);
     vec4 lMVPos=lights[lightIndex].Lp ;
     vec3 s=  normalize(vec3(lMVPos.xyz) - position); //surf to light
     vec3 v= normalize( - position ); // normalize(vec3((-CAM_POS.xyz)  - position  ));  -position eye direction camera space
     vec3 h =  normalize(v+s);

     float outerConeAngle = cos(radians(lights[lightIndex].Ca));//cos_outer_cone_angle

     float cos_cur_angle = dot (-s ,normalize(lights[lightIndex].Ld));  //angle
     float cos_inner_cone_angle =cos(radians(clamp(lights[lightIndex].Cf,0.0,90.0) )); //cos(radians(clamp(cutoff,0.0,90.0) ));
     float cos_inner_minus_outer_angle =  cos_inner_cone_angle - outerConeAngle;

    
     float spot = 0.0;
     spot =  clamp((cos_cur_angle - outerConeAngle) /   cos_inner_minus_outer_angle, 0.0, 1.0);
     float lambertTerm = max( dot(n,s), 0.0);   
     vec3 spec =vec3(0,0,0);
     if(lambertTerm > 0.0)//angle < cutoff_l
     {
      

           float sDotN= max( 0.0 , dot(s, n) );
           vec3 diff=    KD  *  lights[lightIndex].Lc * sDotN ;
           diff=clamp(diff ,0.0 ,1.0);
           spec =  KS * pow( max( 0.0 ,dot(h,n) ) ,  SHININESS);
           spec = clamp(spec ,0.0 ,1.0);

           return spot * lights[lightIndex].Li *  ( spec+diff);

     }else{
           return	lights[lightIndex].Li ;
     }
         

    
}

 vec4 calculateLights(){

   vec3 lightAccum = vec3(0,0,0); 
   for(int i = 0;i < num_lights ; ++i){

      switch(lights[i].Lt){

         case 0:
              lightAccum  += dirlightType(i,Position ,Normal);  //directional light
            break;
        case  1:
              lightAccum  += pointlightType(i,Position ,Normal);// point light
            break;
        case  2:
              lightAccum  += spotlightType(i,Position ,Normal); //spot light
            break;


      }
   }
   return     vec4(lightAccum ,1);
}
//===================  NO LIGHTS TEX TECHNIQUE   ========================//
 subroutine (RenderMode)vec4 texMode(){
    vec2 texelSize = 1.0 / vec2(textureSize(COLOR_MAP_0, 0));
	vec2 screenTexCoords = gl_FragCoord.xy * texelSize;
    vec4 res= texture(COLOR_MAP_0 ,screenTexCoords);
	vec4 alpha;
    res.a *= OPACITY ;

	  switch(ALPHA_ACTIVE){
       case 1: // do alpha  mask //
	     alpha = texture(ALPHA_MAP_0 ,interpolateAtSample(uvsOut , gl_SampleID));
         res.a *= alpha.a;
       break;
       case 2:// do luma mask //
	      alpha = texture(ALPHA_MAP_0 ,interpolateAtSample(uvsOut , gl_SampleID));
          float luma = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
          res.a *=  luma;
       break;
        case 3:// do inverted alpha mask //
          alpha = texture(ALPHA_MAP_0 ,interpolateAtSample(uvsOut , gl_SampleID));
          res.a *=  1 - alpha.a;

       break;
    }
  

    return  vec4(res.rgb  , res.a);


    
}
//===================  LIGHTS TEX TECHNIQUE   ========================//
 subroutine (RenderMode) vec4 texLightMode(){
    
     vec2 texelSize = 1.0 / vec2(textureSize(COLOR_MAP_0, 0));
	 vec2 screenTexCoords = gl_FragCoord.xy * texelSize;
     vec4 res= texture(COLOR_MAP_0 ,screenTexCoords);
  	 vec4 alpha;
	 res.a *= OPACITY ;

     switch(ALPHA_ACTIVE){
       case 1: // do alpha  mask //
	     alpha = texture(ALPHA_MAP_0 ,interpolateAtSample(uvsOut , gl_SampleID));
         res.a *= alpha.a;
       break;
       case 2:// do luma mask //
	      alpha = texture(ALPHA_MAP_0 ,interpolateAtSample(uvsOut , gl_SampleID));
          float luma = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
          res.a *=  luma;
       break;
       case 3:// do inverted alpha mask //
          alpha = texture(ALPHA_MAP_0 ,interpolateAtSample(uvsOut , gl_SampleID));
          res.a *=  1 - alpha.a;

       break;
     }
        res.rgb  *= calculateLights().rgb;
   
    
        return    vec4(res.rgb ,res.a);
  }

  
   //=======================   output  ===================================//

  void main(void){

    OUTPUT=  renderMode();

  }