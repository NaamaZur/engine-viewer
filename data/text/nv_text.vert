/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP_MATRIX;
uniform mat4 MODEL_VIEW_MATRIX;

 out smooth vec3 Normal;
 out smooth vec2 uvsOut;
 out smooth vec3 Position;
out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
     Normal = vec3(normalize(MODEL_VIEW_MATRIX * vec4(normal,0)));
     uvsOut = uvs;
	 Position = vec3(MODEL_VIEW_MATRIX * position);
	 gl_Position =  MVP_MATRIX *  position;//
}
