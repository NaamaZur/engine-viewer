#version 420 core

// Buffer containing a palette of colors to be used to mark primitives by ID
layout (binding = 0, rgba32f) uniform imageBuffer colors;

// The buffer that we will write to
layout (binding = 1, rgba32f) uniform image2D output_buffer;
in vec4 surface_color;
out vec4 color;

void main(void)
{
    // Load a color from the palette buffer based on primitive ID % 256
    vec4 col = imageLoad(colors, gl_PrimitiveID & 255);
   ///  col*=vec4(1,1,255,1);
    // Store the resulting fragment at two locations. First at the fragments
    // window space coordinate shifted left...

	for(int i =-10 ;i <10 ; ++i){
	       
		   	for(int t =-10 ;t <10 ; ++t){

	        imageStore(output_buffer, ivec2(gl_FragCoord.xy) - ivec2(i * 140 , t * 140), col);

			}
	}
	/*
    imageStore(output_buffer, ivec2(gl_FragCoord.xy) - ivec2(300, 0), col);

    // ... then at the location shifted right
    imageStore(output_buffer, ivec2(gl_FragCoord.xy) + ivec2(0, 0), col);

	imageStore(output_buffer, ivec2(gl_FragCoord.xy) + ivec2(300, 0), col);
	*/
}
