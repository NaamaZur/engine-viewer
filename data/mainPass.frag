/*
Author     : Michael Ivanov, IDOMOO.INC 2013
Description:Color Material Vertex Shader.
*/
#version 430 core
//#pragma optimize(off)

/* Not supported on fermi
#extension GL_NV_gpu_shader5 : require 
#extension GL_NV_bindless_texture : require
*/
#define IDM_ALPHA        1
#define IDM_LUMA         2
#define IDM_ALPHA_INVRT  3
#define IDM_LUMA_INVRT   4
//===================  Subroutines ==============================

/* Renders color solid with no lights */
#define IDM_COLOR_TECHNIQUE          0

/* Renders color solid with lights */
#define	IDM_COLOR_LIGHT_TECHNIQUE    1

/* Renders texture with no lights */
#define IDM_TEXTURE_TECHNIQUE        2

/* Renders texture with  lights */
#define IDM_TEXTURE_LIGHT_TECHNIQUE  3


subroutine vec4 RenderMode();
subroutine uniform RenderMode renderMode;

//============================  UNIFORMS  ==================================//
struct LightInfo{

 vec4 Lp;///light position
 vec3 Li;///light intensity
 vec3 Lc;///light color
 int  Lt;///light type
 vec3 Ld;///light direction (for dir and spot lights only )
 float Ca;///cone angle (spot light only)
 float Cf;///cone Feather (spot light only)

};

const int MAX_LIGHTS = 8;

uniform LightInfo lights[MAX_LIGHTS];


layout(binding=0)  uniform sampler2D COLOR_MAP_0;
layout(binding=1)  uniform sampler2D ALPHA_MAP_0;

/**
* If set to 1 the input tex is unpremultiplied
*/
uniform int unpremult = 0; 
/**
* If set to 1 the input tex is premultiplied
*/
uniform int premult;
// material props:
uniform float OPACITY;
uniform vec3 KD;
 
uniform vec3 KS;
uniform float SHININESS;
uniform int num_lights = 0;
 
uniform int ALPHA_ACTIVE;
//===== in ======================================================
 in smooth vec2 uvsOut;
 in      vec3 Position;
 in smooth vec3 Normal;
 in flat vec4 diffuseOut;


//===== out =====================================================
layout(location = 0)out vec4 OUTPUT;




//=============   DIRECTIONAL LIGHT  ===========================//
//subroutine (lightTypeModel)
vec3 dirlightType( int lightIndex,vec3 position , vec3 normal){

	 vec3 n =normalize(normal);
	 vec4 lMVPos=lights[lightIndex].Lp ;
	 vec3 s= normalize(vec3(lMVPos.xyz)-position); //surf to light
	 vec3 v= normalize( - position  ); //   -position eye direction camera space
	
	 vec3 h =  normalize(v+s);

	 vec3 dir = normalize( lights[lightIndex].Ld - lMVPos.xyz );
   


		float sDotN= max( 0.0 , dot(-dir, n) );
		vec3 diff=    KD  *  lights[lightIndex].Lc * sDotN ;
		diff=clamp(diff ,0.0 ,1.0);
		vec3 spec =vec3(0,0,0);

		if(sDotN >0.0){
		   spec =  KS * pow( max( 0.0 ,dot(h,n) ) ,  SHININESS) ;
		   spec=clamp(spec ,0.0 ,1.0);
		}

	return lights[lightIndex].Li *  (diff +spec );
}

//=================  POINT LIGHT  ============================//
//subroutine (lightTypeModel)
vec3 pointlightType( int lightIndex,vec3 position , vec3 normal){

	 vec3 n =  normalize(normal);
	 vec4 lMVPos=lights[lightIndex].Lp ;
	 vec3 s=   normalize(vec3(lMVPos.xyz) - position); //surf to light
	 vec3 v=   normalize( -position  ); //
	 vec3 h =  normalize(v+s);
	
	  if(!gl_FrontFacing)// if (dot(n, v) < 0.0)  ///take care of backface lightning
	 {
		n = -n;

	 }
	
	 float sDotN= max(  dot(s, n) ,0.0  );
	 vec3 diff=    lights[lightIndex].Lc * KD  *  sDotN ;
	 diff=clamp(diff ,0.0 ,1.0);
	 vec3 spec =vec3(0,0,0);
	// vec3 amb = vec3(0) * lights[lightIndex].Li; NOT IN USE FOR NOW 
	 if(sDotN >0.0){
		   spec =  lights[lightIndex].Lc * KS *  pow( max( 0.0 ,dot(h,n) ) , SHININESS  ) ;//
		   spec=clamp(spec ,0.0 ,1.0);
	 }

	 return   lights[lightIndex].Li * ( spec+diff);	
}

//================= SPOT LIGHT   ============================//
//subroutine (lightTypeModel)
//const float cos_outer_cone_angle =56.29; //0.8; //degs
//const float cutoff=43.0; //setting it to be the same as the outer angle eliminates the feather.
//const float exponent=0.1;
vec3 spotlightType( int lightIndex,vec3 position , vec3 normal){

	 vec3 n =normalize(normal);
	 vec4 lMVPos=lights[lightIndex].Lp ;
	 vec3 s=  normalize(vec3(lMVPos.xyz) - position); //surf to light
	 vec3 v= normalize( - position );  
	 vec3 h =  normalize(v+s);

	 float outerConeAngle = cos(radians(lights[lightIndex].Ca));//cos_outer_cone_angle

	 float cos_cur_angle = dot (-s ,normalize(lights[lightIndex].Ld));  //angle
	 float cos_inner_cone_angle =cos(radians(clamp(lights[lightIndex].Cf,0.0,90.0) )); //cos(radians(clamp(cutoff,0.0,90.0) ));
	 float cos_inner_minus_outer_angle =  cos_inner_cone_angle - outerConeAngle;

	
	 float spot = 0.0;
	 spot =  clamp((cos_cur_angle - outerConeAngle) /   cos_inner_minus_outer_angle, 0.0, 1.0);
	 float lambertTerm = max( dot(n,s), 0.0);   
	 vec3 spec =vec3(0,0,0);
	 if(lambertTerm > 0.0)//angle < cutoff_l
	 {
	  

		  float sDotN= max( 0.0 , dot(s, n) );
		  vec3 diff=    KD  *  lights[lightIndex].Lc * sDotN ;
		  diff=clamp(diff ,0.0 ,1.0);

		   spec =  KS * pow( max( 0.0 ,dot(h,n) ) ,  SHININESS);
		   spec = clamp(spec ,0.0 ,1.0);

		   return spot * lights[lightIndex].Li *  ( spec+diff);

	 }else{
		   return	lights[lightIndex].Li ;
	 }
		 

	
}

vec4 calculateLights(){

   vec3 lightAccum = vec3(0,0,0); 
   for(int i = 0;i < num_lights ; ++i){

	  switch(lights[i].Lt){

		 case 0:
			  lightAccum  += dirlightType(i,Position ,Normal);  //directional light
			break;
		case  1:
			  lightAccum  += pointlightType(i,Position ,Normal);// point light
			break;
		case  2:
			  lightAccum  += spotlightType(i,Position ,Normal); //spot light
			break;


	  }
   }
   return   vec4(lightAccum ,1);
}
//===================  NO LIGHTS COLOR TECHNIQUE   ========================//
layout(index = IDM_COLOR_TECHNIQUE)subroutine (RenderMode)
vec4 colorMode(){
	 vec4 res = diffuseOut ;
	 res.a *= OPACITY;
	// return  vec4(res.rgb *  res.a  , res.a );

	 if(premult == 1){
		return  vec4(res.rgb * res.a , res.a);
	}else{
	   return  vec4(res.rgb  , res.a);
	}

}
//===================  LIGHTS COLOR TECHNIQUE   ========================//
layout(index = IDM_COLOR_LIGHT_TECHNIQUE)subroutine (RenderMode)
vec4 colorLightMode(){
	 vec4 res = diffuseOut ;
	 res.a *= OPACITY;

	if(premult == 1){
		return  vec4(res.rgb *  calculateLights().rgb * res.a , res.a);
	}else{
	   return  vec4(res.rgb * calculateLights().rgb , res.a);
	}



}
//===================  NO LIGHTS TEX TECHNIQUE   ========================//
layout(index = IDM_TEXTURE_TECHNIQUE)
subroutine (RenderMode) vec4 texMode(){
 
	vec4 res= texture(COLOR_MAP_0 ,uvsOut);
	if(unpremult == 1){ ///image layer are not premultiplied by default so they should skip this operation
	   res.rgb =   res.rgb / res.a ;  //for text  unpremult  =1 when mask is set.
	}
	//	 res.rgb =   res.rgb / res.a ; 

	 vec4 alpha;
	 res.a *= OPACITY ;
	
		switch(ALPHA_ACTIVE){
	   case IDM_ALPHA: // do alpha  mask //
		 alpha = texture(ALPHA_MAP_0 ,uvsOut);
		  res.a *= alpha.a;
	   break;
	   case IDM_LUMA:// do luma mask //
		  alpha = texture(ALPHA_MAP_0 ,uvsOut);
		  float luma = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
		  res.a *=  luma;
	   break;
	   case IDM_ALPHA_INVRT:// do inverted alpha mask //
		  alpha = texture(ALPHA_MAP_0 ,uvsOut);
		  res.a *=  1 - alpha.a;

	   break;
	   case  IDM_LUMA_INVRT:
		  alpha = texture(ALPHA_MAP_0 ,uvsOut);
		  float lumainvrt = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
		  res.a *=  lumainvrt;
	   break;
	}
	//res.rgb = res.rgb *  res.a;
	if(premult == 1){

	 	return    vec4(res.rgb *  res.a  , res.a)  ;

	}else{
	 
	 	return    vec4(res.rgb  , res.a);
	 
	 
	}
	 
	
}
//===================  LIGHTS TEX TECHNIQUE   ========================//
layout(index = IDM_TEXTURE_LIGHT_TECHNIQUE)subroutine (RenderMode) 
vec4 texLightMode(){
	
	vec4 res =   texture(COLOR_MAP_0 , uvsOut);
	if(unpremult == 1){
	   res.rgb = res.rgb / res.a ;
	}
	vec4 alpha ;
	res.a *= OPACITY ;

	switch(ALPHA_ACTIVE){
	   case IDM_ALPHA: // do alpha  mask //
		 alpha = texture(ALPHA_MAP_0 ,uvsOut);
		 res.a *= alpha.a;
	   break;
	   case IDM_LUMA:// do luma mask //
		  alpha = texture(ALPHA_MAP_0 ,uvsOut);
		  float luma = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
		  res.a *=  luma;
	   break;
	   case IDM_ALPHA_INVRT:// do inverted alpha mask //
		  alpha = texture(ALPHA_MAP_0 ,uvsOut);
		  res.a *=  1 - alpha.a;

	   break;
	   case  IDM_LUMA_INVRT:
		  alpha = texture(ALPHA_MAP_0 ,uvsOut);
		  float lumainvrt = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
		  res.a *=  lumainvrt;
	   break;
	}


		res.rgb *= calculateLights().rgb;
   
		if(premult == 1){
			return vec4(res.rgb * res.a,res.a);
		}else{
			return vec4(res.rgb ,res.a);
		}

}








   //=======================   output  ===================================//

void main(void){

  OUTPUT= renderMode();
  

}


