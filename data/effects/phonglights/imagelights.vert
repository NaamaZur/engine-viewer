#version 420 core

//in vec2 aPosition;
layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MODEL_VIEW_MATRIX;
noperspective out vec2 vTexcoord;
 out smooth vec3 Position;
 out smooth vec3 Normal;
out gl_PerVertex
{
    vec4 gl_Position;
};

/*----------------------------------------------------------------------------*/
void main() {
	 vTexcoord = position.xy * 0.5  + 0.5;
	 Normal = vec3(normalize(MODEL_VIEW_MATRIX * vec4(normal,0)));
	 Position =  vec3(MODEL_VIEW_MATRIX * position);
	 gl_Position = vec4( position.xy ,0.0 , 1.0 );/// vec4(aPosition, 0.0, 1.0);
}