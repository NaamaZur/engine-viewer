/*********************************************************************************
Author     : Michael Ivanov, IDOMOO.INC 2013
Description:Light pass material.Lights supported: Phong point,directional,spot
**********************************************************************************/

#version 420 core

const int MAX_LIGHTS=15;

struct LightInfo{

 vec4 Lp;///light position
 vec3 Li;///light intensity
 vec3 Lc;///light color
 int  Lt;///light type
 vec3 Ld;///light direction (for dir and spot lights only )
 float Ca;///cone angle (spot light only)
 float Cf;///cone Feather (spot light only)

};



uniform LightInfo lights[MAX_LIGHTS];

layout(binding=0) uniform sampler2D COLOR_MAP_0;
layout(binding=1) uniform sampler2D ALPHA_MAP_0;
uniform vec3 KD;// =vec3(1,1,1);
uniform vec3 KA;// =vec3(1);
uniform vec3 KS;// =vec3(1);
uniform float SHININESS;// =1.0;
uniform int num_lights;
uniform float OPACITY;
uniform int ALPHA_ACTIVE;

//===============  INS  =============================//

noperspective in vec2 vTexcoord;
 in smooth vec3 Position;
 in smooth vec3 Normal;

 //===================== OUTS ======================//

layout(location = 0)out vec4 OUTPUT;


//============================== DIRECTIONAL LIGHT TYPE  ============================//

vec3 dirlightType( int lightIndex,vec3 position , vec3 normal){

	 vec3 n =normalize(normal);
	 vec4 lMVPos=lights[lightIndex].Lp ;
	 vec3 s= normalize(vec3(lMVPos.xyz)-position); //surf to light
	 vec3 v= normalize( - position  ); //   -position eye direction camera space
	
	 vec3 h =  normalize(v+s);

	 vec3 dir = normalize( lights[lightIndex].Ld - lMVPos.xyz );
   


		float sDotN= max( 0.0 , dot(-dir, n) );
		vec3 diff=    KD  *  lights[lightIndex].Lc * sDotN ;
		diff=clamp(diff ,0.0 ,1.0);
		vec3 spec =vec3(0,0,0);

		if(sDotN >0.0){
		   spec =  KS * pow( max( 0.0 ,dot(h,n) ) ,  SHININESS) ;
		   spec=clamp(spec ,0.0 ,1.0);
		}

	return lights[lightIndex].Li *  (diff +spec );
}

//==============================  POINT LIGHT TYPE   ================================//

vec3 pointlightType( int lightIndex,vec3 position , vec3 normal){

	 vec3 n =  normalize(normal);
	 vec4 lMVPos=lights[lightIndex].Lp ;
	 vec3 s=   normalize(vec3(lMVPos.xyz) - position); //surf to
	 vec3 v=   normalize( -position  ); //
	 vec3 h =  normalize(v+s);
	
	 if(!gl_FrontFacing)// if (dot(n, v) < 0.0)  ///take care of backface lightning
	 {
		n = -n;

	 }
	
	 float sDotN= max(  dot(s, n) ,0.0  );
	 vec3 diff=    lights[lightIndex].Lc * KD  *  sDotN ;
	 diff=clamp(diff ,0.0 ,1.0);
	 vec3 spec =vec3(0,0,0);
	// vec3 amb = vec3(0) * lights[lightIndex].Li; NOT IN USE FOR NOW 
	 if(sDotN >0.0){
		   spec =  lights[lightIndex].Lc * KS *  pow( max( 0.0 ,dot(h,n) ) , SHININESS  ) ;
		   spec =  clamp(spec ,0.0 ,1.0);
	 }

	 return   lights[lightIndex].Li  * ( spec+diff);	
}

//========================================  SPOT LIGHT TYPE  ============================================//

vec3 spotlightType( int lightIndex,vec3 position , vec3 normal){

	 vec3 n =normalize(normal);
	 vec4 lMVPos=lights[lightIndex].Lp ;
	 vec3 s=  normalize(vec3(lMVPos.xyz) - position); //surf to light
	 vec3 v= normalize( - position ); // normalize(vec3((-CAM_POS.xyz)  - position  ));  -position eye direction camera space
	 vec3 h =  normalize(v+s);

	 float outerConeAngle = cos(radians(lights[lightIndex].Ca));//cos_outer_cone_angle
	 float cos_cur_angle = dot (-s ,normalize(lights[lightIndex].Ld));  //angle
	 float cos_inner_cone_angle =cos(radians(clamp(lights[lightIndex].Cf,0.0,90.0) )); //cos(radians(clamp(cutoff,0.0,90.0) ));
	 float cos_inner_minus_outer_angle =  cos_inner_cone_angle - outerConeAngle;

	
	 float spot = 0.0;
	 spot =  clamp((cos_cur_angle - outerConeAngle) /   cos_inner_minus_outer_angle, 0.0, 1.0);
	 float lambertTerm = max( dot(n,s), 0.0);   
	 vec3 spec =vec3(0,0,0);
	 if(lambertTerm > 0.0)//angle < cutoff_l
	 {
	  

		  float sDotN= max( 0.0 , dot(s, n) );
		  vec3 diff=    KD  *  lights[lightIndex].Lc * sDotN ;
		  diff=clamp(diff ,0.0 ,1.0);

		   spec =  KS * pow( max( 0.0 ,dot(h,n) ) ,  SHININESS);
		   spec = clamp(spec ,0.0 ,1.0);

		   return spot * lights[lightIndex].Li *  ( spec+diff);

	 }else{
		   return	lights[lightIndex].Li ;
	 }
		 
}

vec4 calculateLights(){

   vec3 lightAccum = vec3(0,0,0); 
   for(int i = 0;i < num_lights ; ++i){

	  switch(lights[i].Lt){

		 case 0:
			  lightAccum  += dirlightType(i,Position ,Normal);    //directional light
			break;
		case  1:
			  lightAccum  += pointlightType(i,Position ,Normal);  // point light
			break;
		case  2:
			  lightAccum  += spotlightType(i,Position ,Normal);   //spot light
			break;


	  }
   }

   return   vec4(lightAccum ,1);
}


void main() {

	 ivec2 tsize =  textureSize(COLOR_MAP_0, 0); 
	 vec2 texcoord = gl_FragCoord.xy * (1.0 / vec2(tsize));	  
	
	 vec4 res = texture(COLOR_MAP_0  ,vec2( texcoord.x , texcoord.y )) ;	
	 vec4 alpha ;  //alpha map
	 res.a *= OPACITY ;

	 switch(ALPHA_ACTIVE){
	   case 1: // do alpha  mask //
		 alpha = texture(ALPHA_MAP_0 ,vTexcoord);
		 res.a *= alpha.a;
	   break;
	   case 2:// do luma mask //
		  alpha = texture(ALPHA_MAP_0 ,vTexcoord);
		  float luma = alpha.r * 0.2126  + alpha.g * 0.7152 + alpha.b * 0.0722;
		  res.a *=  luma;
	   break;
		case 3:// do inverted alpha mask //
		  alpha = texture(ALPHA_MAP_0 ,vTexcoord);
		  res.a *=  1 - alpha.a;
	   break;
	 }


	 res.rgb *= calculateLights().rgb;
	 OUTPUT = res;


}