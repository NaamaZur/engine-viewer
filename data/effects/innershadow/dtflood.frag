/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex;

uniform int uStepSize = 1;

out vec4 fResult;

const float kTexLevels = 65536.0;
const float kZero = 0.5 * kTexLevels / (kTexLevels - 1.0);
const float kInfinity = 0.0;

/*----------------------------------------------------------------------------*/
vec2 map(vec2 x) {
	return x * (kTexLevels - 1.0) / kTexLevels * 2.0 - 1.0;
}

/*----------------------------------------------------------------------------*/
vec2 unmap(vec2 x) {
	 return (x + 1.0) * 0.5 * kTexLevels / (kTexLevels - 1.0);
}

/*----------------------------------------------------------------------------*/
void main() {
	ivec2 texSize = textureSize(uInputTex, 0);
	vec2 texelSize = 1.0 / vec2(texSize);
	ivec2 texcoord = ivec2(gl_FragCoord.xy);
	
	vec4 bestSeed = texelFetch(uInputTex, texcoord, 0);
	bestSeed.xy = map(bestSeed.xy);
	bestSeed.zw = map(bestSeed.zw);
	float bbest = length(bestSeed.xy);
	float fbest = length(bestSeed.zw);
	
	for (int i = -1; i < 2; ++i) {
		for (int j = -1; j < 2; ++j) {
			ivec2 offset = ivec2(i, j) * uStepSize;
			vec4 thisSeed = texelFetch(uInputTex, texcoord + offset, 0);
			thisSeed.xy = map(thisSeed.xy);
			thisSeed.zw = map(thisSeed.zw);
			
		//	background
			if (thisSeed.x > -0.99999) {			
				thisSeed.xy += vec2(offset) * texelSize;
				float fthis = length(thisSeed.xy);
				if (fthis < bbest) {
					bestSeed.xy = thisSeed.xy;
					bbest = fthis;
				}
			}
			
		//	foreground
			if (thisSeed.z > -0.99999) {			
				thisSeed.zw += vec2(offset) * texelSize;
				float bthis = length(thisSeed.zw);
				if (bthis < fbest) {
					bestSeed.zw = thisSeed.zw;
					fbest = bthis;
				}
			}
		}
	}
	fResult = vec4(unmap(bestSeed.xy), unmap(bestSeed.zw));
}