/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0) uniform sampler2D uDistanceField;
uniform float uSize;
uniform float uChoke;

noperspective in vec2 vTexcoord;

out vec4 fResult;

const float kTexLevels = 65536.0;
const float kZero = 0.5 * kTexLevels / (kTexLevels - 1.0);
const float kInfinity = 0.0;

/*----------------------------------------------------------------------------*/
vec2 map(vec2 x) {
	return x * (kTexLevels - 1.0) / kTexLevels * 2.0 - 1.0;
}

/*----------------------------------------------------------------------------*/
float aastep(float threshold ,float dist){
   float afwidth = 0.7 * length(vec2(dFdx(dist) , dFdy(dist)));
   return smoothstep(threshold - afwidth ,threshold + afwidth,dist);
}
void main() {		
	vec4 dvec = texture(uDistanceField, vTexcoord);
	dvec.zw =   map(dvec.zw);//zw
   
	float d = clamp((length(dvec.zw) - uSize) / (uSize * uChoke - uSize), 0.0, 1.0);
	 
	fResult =    vec4( smoothstep(1.0, 0.0, d )) ;
	 
}