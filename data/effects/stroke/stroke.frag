/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uSrcTex; // unpremultiplied argb
layout(binding = 1)uniform sampler2D uDistanceTex;

uniform vec3 uColor;
uniform float uSize;
uniform float uSoft; // soft edge to remove aliasing
uniform float uOpacity;
uniform int uPosition;

#define STROKE_POSITION_OUTSIDE		1
#define STROKE_POSITION_INSIDE		2
#define STROKE_POSITION_CENTER		3

noperspective in vec2 vTexcoord;

out vec4 fResult;

const float kTexLevels = 65536.0;
const float kZero = 0.5 * kTexLevels / (kTexLevels - 1.0);
const float kInfinity = 0.0;

/*----------------------------------------------------------------------------*/
vec2 map(vec2 x) {
	return x * (kTexLevels - 1.0) / kTexLevels * 2.0 - 1.0;
}

/*----------------------------------------------------------------------------*/
float getStrokeAlpha(in float dist, in float size, in float soft) {
	return step(dist, size) * smoothstep(size, size - soft, dist);
}

/*----------------------------------------------------------------------------*/
void main() {
	vec2 texelSize = 1.0 / vec2(textureSize(uDistanceTex, 0));
	float tlen = length(texelSize);
	vec4 src = texture(uSrcTex, vTexcoord);//.gbar; // to rgba
		
	vec4 dvec = texture(uDistanceTex, vTexcoord);
	dvec.xy = map(dvec.xy);
	dvec.zw = map(dvec.zw);
	
	switch (uPosition) {
		default:
		case (STROKE_POSITION_OUTSIDE): {
			float alpha = getStrokeAlpha(length(dvec.xy), uSize * tlen, uSoft * tlen);
			alpha *= uOpacity;
			fResult = vec4(
				uColor.rgb * (1.0 - src.a) + src.rgb * src.a,
				max(alpha, src.a)
			);
			break;
		}
		case (STROKE_POSITION_INSIDE): {
			float alpha = getStrokeAlpha(length(dvec.zw), uSize * tlen, uSoft * tlen);
			alpha *= src.a;
			alpha *= uOpacity;
			fResult = vec4(
				uColor.rgb * alpha + src.rgb * (1.0 - alpha),
				max(alpha, src.a)
			);
			break;
		}
		case (STROKE_POSITION_CENTER): {
			float alpha = getStrokeAlpha(length(dvec.xy), uSize * tlen * 0.5, uSoft * tlen);
			alpha *= uOpacity;
			src = vec4(
				uColor.rgb * (1.0 - src.a) + src.rgb * src.a,
				max(alpha, src.a)
			);
			alpha = getStrokeAlpha(length(dvec.zw), uSize * tlen * 0.5, uSoft * tlen);
			alpha *= src.a;
			alpha *= uOpacity;
			fResult = vec4(
				uColor.rgb * alpha + src.rgb * (1.0 - alpha),
				max(alpha, src.a)
			);
			break;
		}
	};
	
	 fResult = fResult;//.argb; // to argb
}