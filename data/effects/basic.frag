#version 420 core

layout(binding=0)uniform sampler2D uInputTex;

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/

void main() {

	fResult = texture(uInputTex, vTexcoord);
 
}