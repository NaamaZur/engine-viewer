/*******************************************************************************
*******************************************************************************/
#version 420 core

#define GRADIENT_LINEAR		1
#define GRADIENT_SMOOTH		2
#define GRADIENT_SHARP		3

#define COMPOSITE_ADD		1
#define COMPOSITE_ALPHA		2
#define COMPOSITE_CUTOUT	3

layout(binding = 0) uniform sampler2D uInputTex; // unpremultiplied argb

uniform vec2 uViewportSize;
uniform float uSweepIntensity;
uniform float uEdgeIntensity;
uniform float uEdgeWidth;
uniform vec4 uColor; // rgba
uniform int uGradientType;
uniform int uCompositeType;

noperspective in vec2 vTexcoord;

out vec4 fResult; // unpremultiplied argb

/*----------------------------------------------------------------------------*/
float gradientLinear(in float x) {
	return 1.0 - abs(x);
}

/*----------------------------------------------------------------------------*/
float gradientSmooth(in float x) {
	return smoothstep(0.0, 1.0, 1.0 - abs(x));
}

/*----------------------------------------------------------------------------*/
float gradientSharp(in float x) {
	float d = 1.0 - abs(x);
	return mix(0.0, 1.0, d * d);
}

/*----------------------------------------------------------------------------*/
void main() {
	float sweep = 0.0;	
	switch (uGradientType) {
		case GRADIENT_SHARP:
			sweep = gradientSharp(vTexcoord.x * 2.0 - 1.0);
			break;
		case GRADIENT_SMOOTH:
			sweep = gradientSmooth(vTexcoord.x * 2.0 - 1.0);
			break;
		case GRADIENT_LINEAR:
		default:
			sweep = gradientLinear(vTexcoord.x * 2.0 - 1.0);
			break;
	};
	
	float edge = 0.0;
	edge += gradientSmooth(gl_FragCoord.x / uEdgeWidth);
	edge += gradientSmooth(gl_FragCoord.y / uEdgeWidth);
	edge += gradientSmooth((uViewportSize.x - gl_FragCoord.x) / uEdgeWidth);
	edge += gradientSmooth((uViewportSize.y - gl_FragCoord.y) / uEdgeWidth);
	edge *= sweep;

	sweep = (sweep * uSweepIntensity) + (sweep * edge * uEdgeIntensity);
	
	vec4 src = vec4(sweep) * uColor;
	vec4 dst = texture(uInputTex, gl_FragCoord.xy / uViewportSize);//.gbar; // to rgba
	switch(uCompositeType) {
		case COMPOSITE_CUTOUT:
			fResult = vec4((src.rgb / src.a), src.a * dst.a);
			break;
		case COMPOSITE_ALPHA:
			fResult = vec4(src.rgb + (1.0 - src.a) * dst.rgb, dst.a);
			break;
		case COMPOSITE_ADD:
		default:
			fResult = vec4(src.rgb + dst.rgb, dst.a);
			break;
	};
	
  //	fResult = fResult.rgba; // to argb
}