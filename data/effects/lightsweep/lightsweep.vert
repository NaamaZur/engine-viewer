/*******************************************************************************

                   LIGHT SWEEP EFFECT

*******************************************************************************/
#version 420 core

//in vec2 aPosition;
layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;


uniform vec2 uViewportSize;
uniform vec2 uSweepCenter;
uniform float uSweepAngle;
uniform float uSweepWidth;

noperspective out vec2 vTexcoord;

out gl_PerVertex
{
    vec4 gl_Position;
};


/*----------------------------------------------------------------------------*/
vec2 ndcToPixels(in vec2 ndc, in vec2 viewportSize) {
	return (ndc * 0.5 + 0.5) * viewportSize;
}

/*----------------------------------------------------------------------------*/
vec2 pixelsToNdc(in vec2 pixels, in vec2 viewportSize) {
	return (pixels / viewportSize) * 2.0 - 1.0;
}

/*----------------------------------------------------------------------------*/
void main() {

	vTexcoord = position.xy * 0.5 + 0.5;

//	construct transformation:
	mat3 s = mat3( // scale for width
		uSweepWidth,		0.0,				0.0,
		0.0,				100.0,				0.0,
		0.0,				0.0,				1.0
	);	
	mat3 t = mat3( // 
		1.0,				0.0,				0.0,
		0.0,				1.0,				0.0,
		uSweepCenter.x,		uSweepCenter.y,		1.0
	);	
	mat3 r = mat3(
		cos(uSweepAngle),	sin(uSweepAngle),	0.0,
		-sin(uSweepAngle),	cos(uSweepAngle),	0.0,		
		0.0,				0.0,				1.0
	);
	mat3 trs = t * r * s;

	vec3 pos = vec3(ndcToPixels(position.xy, uViewportSize), 1.0); // move to image space
	pos.xy -= uViewportSize / 2.0; // translate to viewport center
	pos = trs * pos; // apply sweep transformation
	pos.xy = pixelsToNdc(pos.xy, uViewportSize); // return to NDC space
	
	gl_Position = vec4(pos.xy, 0.0, 1.0);
}