#version 420 core

//in vec2 aPosition;
layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

noperspective out vec2 vTexcoord;


/*----------------------------------------------------------------------------*/
void main() {
	vTexcoord = position.xy * 0.5  + 0.5;
	gl_Position = vec4( position.xy , 0.0 , 1.0 ); 
}