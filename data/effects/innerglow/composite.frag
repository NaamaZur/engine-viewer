/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uGlowTex;
layout(binding = 1)uniform sampler2D uSrcTex; // unpremultiplied argb
uniform float uRange;
uniform vec3 uColor0;
uniform vec3 uColor1;
uniform int uCenter; // 0 = edge, 1 = center
uniform int uBlendMode = 8; // must match one of the #defines below
uniform float uOpacity = 1.0;
uniform float uNoise = 0.0;
uniform float uJitter = 0.0;

noperspective in vec2 vTexcoord;

out vec4 fResult; // unpremultiplied rgba

#define GRADIENT_LINEAR			1
#define GRADIENT_RADIAL			2

#define BLEND_NORMAL			1
#define BLEND_DARKEN			2
#define BLEND_MULTIPLY			3
#define BLEND_COLOR_BURN		4
#define BLEND_LINEAR_BURN		5
#define BLEND_DARKER_COLOR		6
#define BLEND_LIGHTEN			7
#define BLEND_SCREEN			8
#define BLEND_COLOR_DODGE		9
#define BLEND_LINEAR_DODGE		10
#define BLEND_LIGHTER_COLOR		11
#define BLEND_OVERLAY			12
#define BLEND_SOFT_LIGHT		13
#define BLEND_HARD_LIGHT		14
#define BLEND_VIVID_LIGHT		15
#define BLEND_LINEAR_LIGHT		16
#define BLEND_PIN_LIGHT			17
#define BLEND_HARD_MIX			18
#define BLEND_DIFFERENCE		29
#define BLEND_EXCLUSION			20
#define BLEND_HUE				21
#define BLEND_SATURATION		22
#define BLEND_COLOR				23
#define BLEND_LUMINOSITY		24
#define BLEND_SUBTRACT			25
#define BLEND_DIVIDE			26

/*----------------------------------------------------------------------------*/
vec3 luminance(in vec3 color, float delta) {
	vec3 gray = vec3(dot(vec3(0.3, 0.59, 0.11), color));
	return mix(color, gray, delta);
}

/*----------------------------------------------------------------------------*/
vec3 rgbToHsl(in vec3 c) {
	vec4 K = vec4(0.0, -1.0/3.0, 2.0/3.0, -1.0);
	vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
	vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}
/*----------------------------------------------------------------------------*/
vec3 hslToRgb(in vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

#define BlendLinearDodgef 				BlendAddf
#define BlendLinearBurnf 				BlendSubstractf
#define BlendAddf(base, blend) 			min(base + blend, 1.0)
#define BlendSubstractf(base, blend) 	max(base + blend - 1.0, 0.0)
#define BlendLightenf(base, blend) 		max(blend, base)
#define BlendDarkenf(base, blend) 		min(blend, base)
#define BlendLinearLightf(base, blend) 	(blend < 0.5 ? BlendLinearBurnf(base, (2.0 * blend)) : BlendLinearDodgef(base, (2.0 * (blend - 0.5))))
#define BlendScreenf(base, blend) 		(1.0 - ((1.0 - base) * (1.0 - blend)))
#define BlendOverlayf(base, blend) 		(base < 0.5 ? (2.0 * base * blend) : (1.0 - 2.0 * (1.0 - base) * (1.0 - blend)))
#define BlendSoftLightf(base, blend) 	((blend < 0.5) ? (2.0 * base * blend + base * base * (1.0 - 2.0 * blend)) : (sqrt(base) * (2.0 * blend - 1.0) + 2.0 * base * (1.0 - blend)))
#define BlendColorDodgef(base, blend) 	((blend == 1.0) ? blend : min(base / (1.0 - blend), 1.0))
#define BlendColorBurnf(base, blend) 	((blend == 0.0) ? blend : max((1.0 - ((1.0 - base) / blend)), 0.0))
#define BlendVividLightf(base, blend) 	((blend < 0.5) ? BlendColorBurnf(base, (2.0 * blend)) : BlendColorDodgef(base, (2.0 * (blend - 0.5))))
#define BlendPinLightf(base, blend) 	((blend < 0.5) ? BlendDarkenf(base, (2.0 * blend)) : BlendLightenf(base, (2.0 *(blend - 0.5))))
#define BlendHardMixf(base, blend) 		((BlendVividLightf(base, blend) < 0.5) ? 0.0 : 1.0)
#define BlendReflectf(base, blend) 		((blend == 1.0) ? blend : min(base * base / (1.0 - blend), 1.0))

#define Blend(base, blend, funcf) 		vec3(funcf(base.r, blend.r), funcf(base.g, blend.g), funcf(base.b, blend.b))

#define BlendNormal(base, blend) 		(blend)
#define BlendLighten					BlendLightenf
#define BlendDarken						BlendDarkenf
#define BlendMultiply(base, blend) 		(base * blend)
#define BlendAverage(base, blend) 		((base + blend) / 2.0)
#define BlendAdd(base, blend) 			min(base + blend, vec3(1.0))
#define BlendSubtract(base, blend) 		max(base + blend - vec3(1.0), vec3(0.0))
#define BlendDifference(base, blend) 	abs(base - blend)
#define BlendNegation(base, blend) 		(vec3(1.0) - abs(vec3(1.0) - base - blend))
#define BlendExclusion(base, blend) 	(base + blend - 2.0 * base * blend)
#define BlendScreen(base, blend) 		Blend(base, blend, BlendScreenf)
#define BlendOverlay(base, blend) 		Blend(base, blend, BlendOverlayf)
#define BlendSoftLight(base, blend) 	Blend(base, blend, BlendSoftLightf)
#define BlendHardLight(base, blend) 	BlendOverlay(blend, base)
#define BlendColorDodge(base, blend) 	Blend(base, blend, BlendColorDodgef)
#define BlendColorBurn(base, blend) 	Blend(base, blend, BlendColorBurnf)
#define BlendLinearDodge				BlendAdd
#define BlendLinearBurn					BlendSubtract

#define BlendLinearLight(base, blend) 	Blend(base, blend, BlendLinearLightf)
#define BlendVividLight(base, blend) 	Blend(base, blend, BlendVividLightf)
#define BlendPinLight(base, blend) 		Blend(base, blend, BlendPinLightf)
#define BlendHardMix(base, blend) 		Blend(base, blend, BlendHardMixf)
#define BlendReflect(base, blend) 		Blend(base, blend, BlendReflectf)
#define BlendGlow(base, blend) 			BlendReflect(blend, base)
#define BlendPhoenix(base, blend) 		(min(base, blend) - max(base, blend) + vec3(1.0))
#define BlendOpacity(base, blend, F, O) (F(base, blend) * O + blend * (1.0 - O))

/*----------------------------------------------------------------------------*/
vec3 BlendHue(in vec3 base, in vec3 blend) {
	vec3 baseHSL = rgbToHsl(base);
	return hslToRgb(vec3(rgbToHsl(blend).r, baseHSL.g, baseHSL.b));
}

/*----------------------------------------------------------------------------*/
vec3 BlendSaturation(in vec3 base, in vec3 blend) {
	vec3 baseHSL = rgbToHsl(base);
	return hslToRgb(vec3(baseHSL.r, rgbToHsl(blend).g, baseHSL.b));
}

/*----------------------------------------------------------------------------*/
vec3 BlendColor(in vec3 base, in vec3 blend) {
	vec3 blendHSL = rgbToHsl(blend);
	return hslToRgb(vec3(blendHSL.r, blendHSL.g, rgbToHsl(base).b));
}

/*----------------------------------------------------------------------------*/
vec3 BlendLuminosity(in vec3 base, in vec3 blend) {
	vec3 baseHSL = rgbToHsl(base);
	return hslToRgb(vec3(baseHSL.r, baseHSL.g, rgbToHsl(blend).b));
}

/*----------------------------------------------------------------------------*/
float rand(in vec2 seed) {
	return fract(sin(dot(seed.xy, vec2(12.9898, 78.233))) * 43758.5453);
}


/*----------------------------------------------------------------------------*/
void main() {
	vec4 dst = texture(uSrcTex, vTexcoord);//.gbar; // to rgba
	
	float rval = rand(vTexcoord);
	
//	generate glow color
	float glow = texture(uGlowTex, vTexcoord).a;///.r;
	if (!bool(uCenter)) {
		glow = 1.0 - glow;
	}
	float d = smoothstep(0.0, 1.0, clamp(glow / (uRange * 2.0), 0.0, 1.0));
	vec4 result = vec4(
		mix(uColor1, uColor0, d * clamp(rval + 1.0 - uJitter, 0.0, 1.0)),
		glow * uOpacity
	);
	
	if (uColor0 == uColor1){
		result.a *= uRange * 0.5 + 0.5;
	}

	
// apply blending
	switch (uBlendMode) {
		default:
		case BLEND_NORMAL: 
			result.rgb = BlendNormal(dst.rgb, result.rgb);
			break;
		case BLEND_DARKEN:
			result.rgb = BlendDarken(dst.rgb, result.rgb);
			break;
		case BLEND_MULTIPLY:
			result.rgb = BlendMultiply(dst.rgb, result.rgb);
			break;
		case BLEND_COLOR_BURN:
			result.rgb = BlendColorBurn(dst.rgb, result.rgb);
			break;
		case BLEND_LINEAR_BURN:
			result.rgb = BlendLinearBurn(dst.rgb, result.rgb);
			break;
		case BLEND_DARKER_COLOR:
			result.rgb = BlendDarken(dst.rgb, result.rgb);
			break;
		case BLEND_LIGHTEN:
			result.rgb = BlendLighten(dst.rgb, result.rgb);
			break;
		case BLEND_SCREEN:
			result.rgb = BlendScreen(dst.rgb, result.rgb);
			break;
		case BLEND_COLOR_DODGE:
			result.rgb = BlendColorDodge(dst.rgb, result.rgb);
			break;
		case BLEND_LINEAR_DODGE:
			result.rgb = BlendLinearDodge(dst.rgb, result.rgb);
			break;
		case BLEND_LIGHTER_COLOR:
			result.rgb = BlendLighten(dst.rgb, result.rgb);
			break;
		case BLEND_OVERLAY:
			result.rgb = BlendOverlay(dst.rgb, result.rgb);
			break;
		case BLEND_SOFT_LIGHT:
			result.rgb = BlendSoftLight(dst.rgb, result.rgb);
			break;
		case BLEND_HARD_LIGHT:
			result.rgb = BlendHardLight(dst.rgb, result.rgb);
			break;
		case BLEND_VIVID_LIGHT:
			result.rgb = BlendVividLight(dst.rgb, result.rgb);
			break;
		case BLEND_LINEAR_LIGHT:
			result.rgb = BlendLinearLight(dst.rgb, result.rgb);
			break;
		case BLEND_PIN_LIGHT:
			result.rgb = BlendPinLight(dst.rgb, result.rgb);
			break;
		case BLEND_HARD_MIX:
			result.rgb = BlendHardMix(dst.rgb, result.rgb);
			break;
		case BLEND_DIFFERENCE:
			result.rgb = BlendDifference(dst.rgb, result.rgb);
			break;
		case BLEND_EXCLUSION:
			result.rgb = BlendExclusion(dst.rgb, result.rgb);
			break;
		case BLEND_HUE:
			result.rgb = BlendHue(dst.rgb, result.rgb);
			break;
		case BLEND_SATURATION:
			result.rgb = BlendSaturation(dst.rgb, result.rgb);
			break;
		case BLEND_COLOR:
			result.rgb = BlendColor(dst.rgb, result.rgb);
			break;
		case BLEND_LUMINOSITY:
			result.rgb = BlendLuminosity(dst.rgb, result.rgb);
			break;
		case BLEND_SUBTRACT:
			result.rgb = BlendSubtract(dst.rgb, result.rgb);
			break;
		case BLEND_DIVIDE:
			result.rgb = BlendReflect(dst.rgb, result.rgb);
			break;
	};

//	apply opacity
	result.a = clamp(1.0 - uOpacity + result.a, 0.0, 1.0);
	result.rgb = mix(result.rgb, dst.rgb, result.a * clamp(rval + 1.0 - uNoise, 0.0, 1.0));
	result.a = dst.a;
	
	fResult = result;//.argb; // to argb
}