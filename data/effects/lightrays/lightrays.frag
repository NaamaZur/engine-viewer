/*******************************************************************************
             LIGHTRAYS EFFECT 
*******************************************************************************/
#version 420 core

#define SHAPE_ROUND			1
#define SHAPE_SQUARE		2

#define COMPOSITE_NONE		1
#define COMPOSITE_ADD		2
#define COMPOSITE_LIGHTEN	3
#define COMPOSITE_SCREEN	4

layout(binding = 0) uniform sampler2D uInputTex; // unpremultiplied argb

uniform float uIntensity;
uniform vec2 uCenter;
uniform float uRadius;
uniform float uSoftness;
uniform int uShapeType;
uniform vec2 uDirection;
uniform int uColorFromSource;
uniform int uAllowBrighten;
uniform vec4 uColor; // rgba
uniform int uCompositeType;

noperspective in vec2 vTexcoord; // unpremultiplied argb

out vec4 fResult;

/*----------------------------------------------------------------------------*/
float lengthRound(in vec2 x) {
	return length(x);
}
/*----------------------------------------------------------------------------*/
float lengthSquare(in vec2 x) {
	mat2 rmat = mat2(
		uDirection.x,	-uDirection.y,
		uDirection.y,	uDirection.x
	);
	vec2 rx = rmat * x;
	return max(abs(rx.x), abs(rx.y));
}

/*----------------------------------------------------------------------------*/
float accel(in float a, in float b, in float delta) {
	return mix(a, b, delta * delta);
}
/*----------------------------------------------------------------------------*/
float decel(in float a, in float b, in float delta) {
	float rdelta = clamp(1.0 - delta, 0.0, 1.0);
	return mix(a, b, 1.0 - (rdelta * rdelta));
}

/*----------------------------------------------------------------------------*/
vec4 composite(in vec4 src, in vec4 dst) {
	vec3 psrc = src.rgb * src.a; // premultiply
	vec3 pdst = dst.rgb * dst.a; // premultiply
	vec4 result;	
	switch (uCompositeType) {
		case COMPOSITE_SCREEN:
			result.rgb = 1.0 - (1.0 - psrc) * (1.0 - pdst);
			break;
		case COMPOSITE_LIGHTEN:
			result.rgb = max(psrc, pdst);
			break;
		case COMPOSITE_ADD:
			result.rgb = psrc + pdst;
			break;
		case COMPOSITE_NONE:
		default:
			return src;
	};
	
	result.a = clamp(max(src.a, dst.a), 0.0, 1.0);
	result.rgb /= result.a; // unpremultiply
	
	return result;
}

/*----------------------------------------------------------------------------*/
void main() {
	vec2 texSize = vec2(textureSize(uInputTex, 0));

//	get ray sample:	
	vec2 warpDirection = (vTexcoord * texSize) - uCenter;
	float warpScale;
	vec2 offset;
	float falloff;
	float brighten;
	switch (uShapeType) {
		case SHAPE_SQUARE  :
			warpScale = pow(lengthSquare(warpDirection / texSize), -(1.0 - uSoftness)) * uRadius;
			offset = (warpDirection / texSize) * warpScale / texSize;
			falloff = decel(1.0, 0.0, lengthSquare(warpDirection) / (uRadius * 20.0));
			brighten = mix(0.5, 0.0, lengthSquare(warpDirection) / uRadius);
			break;
		case SHAPE_ROUND :
		default:
			warpScale = pow(lengthRound(warpDirection / texSize), uSoftness) * uRadius;
			offset = normalize(warpDirection) * warpScale / texSize;
			falloff = decel(1.0, 0.0, lengthRound(warpDirection) / (uRadius * 20.0));
			brighten = mix(0.5, 0.0, lengthRound(warpDirection) / uRadius);
			break;
	};	
	vec4 raySample = texture(uInputTex, (uCenter / texSize) + offset); //.gbar to rgba
	
//	apply intensity/falloff/brighten:
	raySample.a *= uIntensity; // only alpha
	raySample.a *= falloff * decel(1.0, 0.5, uSoftness); // only alpha
	if (bool(uAllowBrighten)) {
		brighten = clamp(brighten * decel(1.0, 0.0, uSoftness), 0.0, 1.0);
		raySample.rgb += raySample.rgb * brighten; // only color
	}
	
//	get source sample:
	vec4 sourceSample = texture(uInputTex, vTexcoord);//.gbar; // to rgba
	if (!bool(uColorFromSource)) {
		raySample.rgb = uColor.rgb;
	}

//	composite:	
	fResult = composite(raySample, sourceSample);//.argb; // to argb
}