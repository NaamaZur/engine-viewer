#version 420 core

layout(binding = 0)uniform sampler2D uSrcTex; // premultiplied rgba
layout(binding = 1)uniform sampler2D uDstTex; // unpremultiplied argb

noperspective in vec2 vTexcoord;

out vec4 fResult; // premultiplied rgba

/*----------------------------------------------------------------------------*/
void main() {
	vec4 src = texture(uSrcTex, vTexcoord).gbar;
	src.rgb *= src.a;
	vec4 dst = texture(uDstTex, vTexcoord).gbar;
	dst.rgb *= dst.a;
	
	vec4 result = mix(dst, src, src.a);
	result.rgb /= result.a; // unpremultiply
	
	fResult = result;//.argb; // to argb
}