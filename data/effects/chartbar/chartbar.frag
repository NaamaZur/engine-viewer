#version 420 core

#define CAP_SHAPE_ROUND				1
#define CAP_SHAPE_SQUARE			2
#define CAP_SHAPE_TRIANGLE			3
#define CAP_SHAPE_HALF_TRIANGLE		4
#define CAP_SHAPE_ONION				5
#define CAP_SHAPE_HALF_ONION		6

layout(binding = 0)uniform sampler2D uFillTex;
uniform int uUseFillTex;
layout(binding = 1)uniform sampler2D uStrokeTex;
uniform int uUseStrokeTex;

uniform ivec2 uViewportSize;
uniform ivec2 uSize; // x = length (bar value), y = width (bar thickness)
uniform float uSoftEdge = 2.0; // for antialising

uniform vec4 uColor0 = vec4(0.0, 0.0, 1.0, 1.0);
uniform vec4 uColor1 = vec4(1.0, 1.0, 1.0, 1.0);
uniform float uFillOpacity = 1.0;

uniform int uCapShape;
uniform float uCapSize; // in [0,2]

uniform int uStrokeWidth = 1;
uniform vec4 uStrokeColor = vec4(1.0, 0.0, 0.0, 1.0);
uniform float uStrokeOpacity = 1.0;

noperspective in vec2 vTexcoord;
noperspective in vec2 vPixcoord;

out vec4 fResult; // unpremultiplied ARGB

/*----------------------------------------------------------------------------*/
/*	Return the distance between point and the edge of a box. The returned 
	distance is signed; negative values are inside the box. */
float boxDistance(
	in vec2 point, // relative to the center of the box
	in vec2 size // x/y = width/height
) {
	vec2 d = abs(point) - size;
	return min(max(d.x, d.y), 0.0) + length(max(d, vec2(0.0)));
}

/*----------------------------------------------------------------------------*/
/*	Return a distance to the edge of a shape, in pixels. */
float capDistance(
	in vec2 coord, // in pixels
	in vec2 size, // size of the cap region in pixels
	in int shape // must match one of the CAP_SHAPE_ defines above
) {
	float edgeDistance = 0.0;
	switch (shape) {
		default:
		case CAP_SHAPE_SQUARE: {
		//	get size of box/position of box (skew to cut off right-hand edge)
			vec2 boxSize = vec2(size.x * 2.0, size.y);
			vec2 boxPoint = coord * 2.0 - boxSize;
			boxPoint.x += boxSize.x;
			edgeDistance = -boxDistance(boxPoint, boxSize);
			break;
		}
		
		case CAP_SHAPE_TRIANGLE: {
		//	lerp over horizontal to get a straight slope (edgeHeight)
			float halfy = size.y * 0.5;
			float edgeHeight = mix(0.0, 1.0, coord.x / size.x) * halfy;
			
			if (coord.y > halfy) {
				edgeDistance = ((halfy - edgeHeight) + halfy) - coord.y;
			} else {
				edgeDistance = coord.y - edgeHeight;
			}
			edgeDistance *= 2.0;
			break;
		}
		
		case CAP_SHAPE_HALF_TRIANGLE: {
		//	lerp over horizontal to get a straight slope (edgeHeight)
			float edgeHeight = (1.0 - (coord.x / size.x)) * size.y;
			//float edgeWidth = (1.0 - (coord.y / size.y)) * size.x;
			//edgeDistance = edgeWidth - coord.x;
			edgeDistance = edgeHeight - coord.y;
			edgeDistance = min(size.y - (size.y - coord.y), edgeDistance);
				
			edgeDistance *= 2.0;
			break;
		}
		
		case CAP_SHAPE_ROUND: {
		//	get size of box/position of box (skew to cut off right-hand edge)
			vec2 boxSize = vec2(size.x * 2.0, size.y);
			vec2 boxPoint = coord * 2.0 - boxSize;
			boxPoint.x += boxSize.x;
			
		//	get rounded corner size, adjust box size
			float r = size.y * (size.x / 200.0);
			boxSize -= vec2(r);
			
		//	subtract r from distance to get rounded corners
			edgeDistance = -(boxDistance(boxPoint, boxSize) - r);
			break;
		}
		
		case CAP_SHAPE_ONION: {
		//	smoothstep over horizontal to get a curved slope (edgeHeight)
			float halfy = size.y * 0.5;
			float edgeHeight = smoothstep(0.0, 1.0, coord.x / size.x) * halfy;
			
			if (coord.y > halfy) {
				edgeDistance = ((halfy - edgeHeight) + halfy) - coord.y;
			} else {
				edgeDistance = coord.y - edgeHeight;
			}
			edgeDistance *= 2.0;
			break;
		}
		
		case CAP_SHAPE_HALF_ONION: {
		//	smoothstep over horizontal to get a curved slope (edgeHeight)
			float edgeHeight = smoothstep(1.0, 0.0, coord.x / size.x) * size.y;
			edgeDistance = edgeHeight - coord.y;
			edgeDistance = min(size.y - (size.y - coord.y), edgeDistance);
				
			edgeDistance *= 2.0;
			break;
		}
	};
	
	return edgeDistance;
}

/*----------------------------------------------------------------------------*/
void main() {
	vec2 size = vec2(uSize) / vec2(uViewportSize);
	
//	get distance field
	float edgeDistance = 1.0;
	if (int(vPixcoord.x) < (uSize.x - uCapSize)) {
	//	fragment is not in cap, use box distance
		vec2 tc = vPixcoord * 2.0 - vec2(uSize);
		edgeDistance = -boxDistance(tc, vec2(uSize)); // negate for inside = positive
	} else {
	//	fragment is in cap, use cap distance
		vec2 coord = vec2(vPixcoord.x - float(uSize.x - uCapSize), vPixcoord.y);
		edgeDistance = capDistance(coord, vec2(uCapSize, uSize.y), uCapShape);
	}
	edgeDistance /= 2.0;

//	get fill color
	vec4 color = vec4(mix(uColor0.rgb, uColor1.rgb, vTexcoord.x), uFillOpacity);
	if (bool(uUseFillTex)) {
	//	get color from texture
		vec2 tc = gl_FragCoord.xy / vec2(uViewportSize);
		color.rgb = texture(uFillTex, tc).gba; // to rgba
	}
	
//	apply stroke
	if (abs(edgeDistance) < uStrokeWidth) {
	//	stroke alpha (for antialiasing)
		float strokeAlpha = (uStrokeWidth - abs(edgeDistance)) / uSoftEdge;
		strokeAlpha = clamp(strokeAlpha, 0.0, 1.0);
		strokeAlpha = strokeAlpha * color.a * uStrokeOpacity;
		
	//	get stroke color
		vec3 strokeColor = uStrokeColor.rgb;
		if (bool(uUseStrokeTex)) {
		//	get color from texture
			vec2 tc = gl_FragCoord.xy / vec2(uViewportSize);
			strokeColor = texture(uStrokeTex, tc).gba; // to rgba
		}		
		color.rgb = mix(color.rgb, strokeColor, strokeAlpha);
		
	}
	
//	apply antialiasing
	float edgeAlpha = edgeDistance / uSoftEdge;
	edgeAlpha = clamp(edgeAlpha, 0.0, 1.0);
	color.a *= edgeAlpha;

	//fResult = vec4(color.rgb * color.a, color.a);
	fResult = vec4(color.rgb, color.a);//.argb;
}