#version 420 core

layout(location = 0)  in vec4 position;

uniform ivec2 uViewportSize;
uniform ivec2 uPosition; // layer space x/y position
uniform float uOrientation;
uniform ivec2 uSize; // x = length (bar value), y = width (bar thickness)
uniform int uFlip = 1; // flip about the bar's y axis

noperspective out vec2 vTexcoord; // texcoord in "bar space" [0,1]
noperspective out vec2 vPixcoord; // texcoord, expressed in pixels

/*----------------------------------------------------------------------------*/
void main() {
	vTexcoord = position.xy * 0.5 + 0.5;
	if (bool(uFlip)) {
		vTexcoord.y = 1.0 - vTexcoord.y;
	}
	vPixcoord = vTexcoord * uSize;
	
	mat3 r = mat3(
		cos(uOrientation),	sin(uOrientation),	0.0,
		-sin(uOrientation),	cos(uOrientation),	0.0,		
		0.0,				0.0,				1.0
	);
	vec3 pos = vec3(position.xy, 1.0);
	pos.xy *= uSize / 2.0;
	pos.x += uSize.x / 2.0;	
	pos = r * pos;	
	pos.xy += uPosition;
	
	pos.xy = pos.xy / uViewportSize * 2.0 - 1.0;
	
	
		
	gl_Position = vec4(pos.xy, 0.0, 1.0);
}