
#version 420 core

layout(binding = 0)uniform sampler2D uDistanceField;
uniform float uSize;

noperspective in vec2 vTexcoord;

out vec4 fResult; // outer/inner height map in r/g channel

const float kTexLevels = 65536.0;
const float kZero = 0.5 * kTexLevels / (kTexLevels - 1.0);
const float kInfinity = 0.0;

/*----------------------------------------------------------------------------*/
vec2 map(vec2 x) {
	return x * (kTexLevels - 1.0) / kTexLevels * 2.0 - 1.0;
}

/*----------------------------------------------------------------------------*/
void main() {
	vec2 texelSize = 1.0 / vec2(textureSize(uDistanceField, 0));
	vec4 dvec = texture(uDistanceField, vTexcoord);
	
	dvec.xy = map(dvec.xy);
	dvec.zw = map(dvec.zw);
	
	float size = length(texelSize * uSize);	
	fResult = vec4(
		clamp(length(dvec.xy) / size, 0.0, 1.0),
		clamp(length(dvec.zw) / size, 0.0, 1.0),
		0.0,
		0.0
	);		
}