/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uSrcTex; // unpremultiplied rgba
layout(binding = 1)uniform sampler2D uDstTex; // unpremultiplied argb

uniform float uBlend;
noperspective in vec2 vTexcoord;
out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	vec4 src = texture(uSrcTex, vTexcoord);
	vec4 dst = texture(uDstTex, vTexcoord);//.gbar; // to rgba
	
	vec4 result = src * (1.0 - uBlend) + dst;
	result.a = dst.a;	
	
	fResult = result;// result.argb; // to argb
}