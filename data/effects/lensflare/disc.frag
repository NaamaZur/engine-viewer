/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform float uBrightness; // should pre-clamp to max brightness

 
uniform vec4 uColor;
uniform vec2 uFalloff; // x = width from edge, y = width from center

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	float delta = length(vTexcoord * 2.0 - 1.0);
	
	float edge = (delta - uFalloff.x) / (1.0 - uFalloff.x);
	edge = clamp(edge, 0.0, 1.0);
	
	float center = ((1.0 - delta) - uFalloff.y) / (1.0 - uFalloff.y);
	center = clamp(center, 0.0, 1.0);

	float falloff = smoothstep(1.0, 0.0, edge);
	falloff *= smoothstep(1.0, 0.0, center);
	
	falloff *= uBrightness;
	
	fResult = uColor * falloff;
}