/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform float uBrightness; // should pre-clamp to max brightness

 
uniform vec4 uColor;

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	float delta = length(vTexcoord * 2.0 - 1.0);
	float falloff = pow(1.0 - delta, 4.0) * uBrightness;	
	fResult = uColor * falloff;
}