/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform vec2 uCenter;
uniform float uAspect;

uniform float uDistance;
uniform float uRadius;
uniform vec2 uViewportSize; // in pixels
uniform float uAngle; // from y axis in radians

///in vec2 aPosition;
layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;
noperspective out vec2 vTexcoord;

out gl_PerVertex
{
	vec4 gl_Position;
};

/*----------------------------------------------------------------------------*/
void main() {
	vTexcoord = position.xy * 0.5 + 0.5;
	
	vec2 positionP = position.xy * uRadius;
	positionP.x = positionP.x / length(uViewportSize) * (2.0 / uRadius);
	mat3 r = mat3(
		cos(uAngle),	sin(uAngle),	0.0,
		-sin(uAngle),	cos(uAngle),	0.0,		
		0.0,			0.0,			1.0
	);
	positionP = (r * vec3(positionP, 1.0)).xy;
	positionP.y *= uAspect;
	positionP += uCenter;
	positionP += (vec2(0.0) - uCenter) * uDistance;
	
	gl_Position = vec4(positionP, 0.0, 1.0);
}