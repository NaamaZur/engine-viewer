/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform vec2 uCenter;
uniform float uAspect;

uniform float uDistance;
uniform float uRadius;

//in vec2 aPosition;
layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;


noperspective out vec2 vTexcoord;

out gl_PerVertex
{
	vec4 gl_Position;
};

/*----------------------------------------------------------------------------*/
void main() {
	vTexcoord = position.xy * 0.5 + 0.5;
	vec2 positionP = position.xy * uRadius;
	positionP.x /= uAspect;
	positionP += uCenter;
	positionP += (vec2(0.0) - uCenter) * uDistance;	
	gl_Position = vec4(positionP, 0.0, 1.0);
}