/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform float uBrightness; // should pre-clamp to max brightness

 
uniform vec4 uColor;


noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	float delta = length(vTexcoord * 2.0 - 1.0);
	float falloff = (1.0 - delta) * uBrightness;
	
	
//	edge falloff for antialiasing (not required)
	//falloff *= 1.0 - abs(vTexcoord.x * 2.0 - 1.0);
	
	fResult = uColor * falloff;
}