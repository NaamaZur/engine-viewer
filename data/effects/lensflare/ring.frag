/*******************************************************************************
*******************************************************************************/
#version 420 core

uniform float uBrightness; // should pre-clamp to max brightness

uniform float uRadius;
uniform vec4 uColor;
uniform vec2 uFalloff; // x = ring width

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	float delta = length(vTexcoord * 2.0 - 1.0);
		
	float halfSize = uFalloff.x / 2.0;
		
	float inside = smoothstep(0.0, 1.0, (delta - (uRadius - uFalloff.x)) / halfSize);
	inside = clamp(inside, 0.0, 1.0);
	
	float outside = smoothstep(1.0, 0.0, (delta - (uRadius - halfSize)) / halfSize);
	outside = clamp(outside, 0.0, 1.0);
	
	fResult = (uColor ) * (outside * inside * (uBrightness) ) ;
}