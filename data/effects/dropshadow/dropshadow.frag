/*******************************************************************************
*******************************************************************************/
//#define PREMULT_INPUT 1
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex; // unpremultiplied argb
layout(binding = 1)uniform sampler2D uShadowTex; // unpremultiplied argb

uniform vec2 uScale = vec2(1.0);
uniform vec2 uBias  = vec2(0.0);

noperspective in vec2 vTexcoord;

out vec4 fResult; // unpremultiplied argb

/*----------------------------------------------------------------------------*/
void main() {
	   vec4 src = texture(uInputTex, vTexcoord  );//.gbar; // to rgba
#ifdef PREMULT_INPUT
	   src.rgb/= src.a;
#endif
	vec4 dst = texture(uShadowTex, vTexcoord * uScale + uBias   );//.gbar; // to rgba
	 
	src.rgb *= src.a; // to premultiplied
	dst.rgb *= dst.a; // to premultiplied
	
	fResult = src * src.a + dst * (1.0 - src.a); // blend
	
	 fResult.rgb /= fResult.a; // to unpremultiplied
	 
}