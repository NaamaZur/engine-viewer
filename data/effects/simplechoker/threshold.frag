/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex;
uniform vec4 uThreshold = vec4(0.0);

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	vec4 result = texture(uInputTex, vTexcoord);
	result *= step(uThreshold, result);
	fResult = result;
}