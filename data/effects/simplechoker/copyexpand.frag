/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex;
uniform vec2 uOutSize;

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	vec2 inSize = vec2(textureSize(uInputTex, 0));
	
	vec2 s = uOutSize / inSize;
	vec2 b = (inSize - uOutSize) * (0.5 / inSize);
	
	fResult = texture(uInputTex, vTexcoord * s + b);
}