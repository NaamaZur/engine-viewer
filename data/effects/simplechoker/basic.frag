/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex;

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	float alpha = texture(uInputTex, vTexcoord).a;
	fResult = vec4(vec3(alpha) , 1.0);
}