/*******************************************************************************
*******************************************************************************/
// FXAA shader, GLSL code adapted from:
// http://horde3d.org/wiki/index.php5?title=Shading_Technique_-_FXAA
// Whitepaper describing the technique:
// http://developer.download.nvidia.com/assets/gamedev/files/sdk/11/FXAA_WhitePaper.pdf


#version 420 core

layout(binding = 0)uniform sampler2D uInputTex; // unpremultiplied argb

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
/*	Note that this assumes the alpha channel of uInputTex is in the RED channel
	(uInputTex is ARGB). To convert to RGBA, just swap .r for .a on the texture
	reads below. */
float fxaa(in vec2 origin, in sampler2D tex) {
	#define FXAA_REDUCE_MIN   (1.0/128.0)
	#define FXAA_REDUCE_MUL   (1.0/16.0)
	#define FXAA_SPAN_MAX     8.0
	
	vec2 texelSize = 1.0 / vec2(textureSize(tex, 0));
	vec4 posPos = vec4(origin, origin - texelSize * (0.5 + 1.0/4.0));

//	sample neighborhood:
	float s[5];
	s[0] = textureLod(tex, posPos.xy, 0.0).a;
	s[1] = textureLod(tex, posPos.zw, 0.0).a;
	s[2] = textureLodOffset(tex, posPos.zw, 0.0, ivec2(1, 0)).a;
	s[3] = textureLodOffset(tex, posPos.zw, 0.0, ivec2(0, 1)).a;
	s[4] = textureLodOffset(tex, posPos.zw, 0.0, ivec2(1, 1)).a;
	
//	get min/max:
	float smin = min(s[0], min(min(s[1], s[2]), min(s[3], s[4])));
	float smax = max(s[0], max(max(s[1], s[2]), max(s[3], s[4])));

//	estimate edge:
	vec2 dir = vec2(
		-((s[1] + s[2]) - (s[3] + s[4])),
		((s[1] + s[3]) - (s[2] + s[4]))
	);
	
	float dirReduce = max(
		(s[1] + s[2] + s[3] + s[4]) * (0.25 * FXAA_REDUCE_MUL),
		FXAA_REDUCE_MIN
	);
    float dirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);
	dir = min(
		vec2(FXAA_SPAN_MAX), 
		max(vec2(-FXAA_SPAN_MAX), dir * dirMin)
	) * texelSize.xy;
	
	float resultA = (textureLod(tex, posPos.xy + dir * (1.0/3.0 - 0.5), 0.0).a + textureLod(tex, posPos.xy + dir * (2.0/3.0 - 0.5), 0.0).a) * 0.5;
	float resultB = resultA * (1.0/2.0) + (1.0/4.0) * (textureLod(tex, posPos.xy + dir * -0.5, 0.0).a + textureLod(tex, posPos.xy + dir * 0.5, 0.0).a);
	if ((resultB < smin) || (resultB > smax)) {
		return resultA;
		}
    return resultB;
}

/*----------------------------------------------------------------------------*/    
void main() {
	fResult =   vec4(fxaa(vTexcoord, uInputTex));
}