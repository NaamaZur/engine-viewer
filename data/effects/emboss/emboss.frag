/*******************************************************************************
*******************************************************************************/
//#define PREMULT_INPUT 1
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex;

uniform vec2 uDirection; // incorporates 'relief' amount
uniform float uContrast;
uniform float uBlend;

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	vec2 texelSize = 1.0 / vec2(textureSize(uInputTex, 0));
	vec2 dirr = vec2(0.0f,1.0f);
	vec2 dvec = uDirection  * texelSize;//
	
	vec4 src = texture(uInputTex, vTexcoord);
#ifdef PREMULT_INPUT
	src.rgb/= src.a; //UNPREMULTIPLYING PREMULTIPLIED INPUT	
#endif
//	get hilight:
	vec4 hi = texture(uInputTex, vTexcoord + dvec);
#ifdef PREMULT_INPUT
	hi.rgb/=hi.a;    //UNPREMULTIPLYING PREMULTIPLIED INPUT	
#endif
//	hi.gba *= hi.r;
	hi.rgb *= hi.a;
//	hi.gba = clamp(hi.gba, 0.0, 1.0);
	hi.rgb = clamp(hi.rgb ,0.0, 1.0);
//	get lolight:
	vec4 lo = texture(uInputTex, vTexcoord - dvec);
#ifdef PREMULT_INPUT
	lo.rgb/=lo.a;   //UNPREMULTIPLYING PREMULTIPLIED INPUT	
#endif
	//lo.gba *= lo.r;
	lo.rgb *= lo.a;
	//lo.gba = 1.0 - lo.gba;
	lo.rgb = 1.0 - lo.rgb;
//	lo.gba = clamp(lo.gba, 0.0, 1.0);
	lo.rgb = clamp(lo.rgb,0.0,1.0);
//	mix hi/lo:
//	vec3 emboss = (hi.gba + lo.gba) * 0.5;
	vec3 emboss = (hi.rgb + lo.rgb) *0.5;
	emboss = (emboss - 0.5) * uContrast + 0.5;
	
//	mix with src:
//	fResult = vec4(src.r, src.gba * uBlend + emboss * (1.0 - uBlend));
	fResult = vec4(src.rgb * uBlend + emboss * (1.0 - uBlend),src.a);
}