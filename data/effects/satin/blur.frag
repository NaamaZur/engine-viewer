 
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex; // unpremultiplied argb

#define MAX_BLUR_RADIUS 4096

uniform int uBlurriness;
uniform vec2 uBlurDirection; // (1,0)/(0,1) for x/y pass

noperspective in vec2 vTexcoord; // unpremultiplied argb

out vec4 fResult;

/*----------------------------------------------------------------------------*/
/*	Based on the incremental Gaussian blur from other effects. Note the addition
	of kPow.*/
float blur(
	in sampler2D srcTex, 
	in vec2 srcTexelSize, 
	in vec2 origin,
	in float radius,
	in vec2 direction
) {
	int nSamples = clamp(int(radius), 1, int(MAX_BLUR_RADIUS));
	if (nSamples <= 2){
		return texture(srcTex, origin).a;//.r;
	}
	float kSigma		= float(radius) / 3.0;
	float kSigma2		= kSigma * kSigma;
	const float kTau	= 6.2831853071795;
	const float kPow	= 0.96;
		
//	set up incremental counter:
	vec3 gaussInc;
	gaussInc.x = 1.0 / (sqrt(kTau) * kSigma);
	gaussInc.y = exp(-0.5 / kSigma2);
	gaussInc.z = gaussInc.y * gaussInc.y;
	
//	accumulate results:
	float result = texture(srcTex, origin).a * gaussInc.x;
	for (int i = 1; i < nSamples; ++i) {
		gaussInc.xy *= gaussInc.yz;
		
		vec2 offset = float(i) * direction * srcTexelSize;
		result += pow(texture(srcTex, origin - offset).a * gaussInc.x, kPow);
		result += pow(texture(srcTex, origin + offset).a * gaussInc.x, kPow);
	}
	
	return result;
}

/*----------------------------------------------------------------------------*/
void main() {
	fResult =vec4(blur(
		uInputTex, 
		1.0 / vec2(textureSize(uInputTex, 0)), 
		vTexcoord, 
		float(uBlurriness), 
		uBlurDirection
	));
}
