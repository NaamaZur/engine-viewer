/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0) uniform sampler2D uInputTex; // unpremultiplied argb

uniform vec2 uOffset;

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
void main() {
	const float kIntMax = 255.0; // assumes uInputTex is 8bpc

	int left = int(texture(uInputTex, vTexcoord - uOffset).a * kIntMax); // alpha in red
	int right = int(texture(uInputTex, vTexcoord + uOffset).a * kIntMax); // alpha in red
	
	fResult = vec4(float(left ^ right) / kIntMax);
}