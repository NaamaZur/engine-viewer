
#version 420 core

layout(binding = 0)uniform sampler2D uDistanceField;
uniform int uSize; // in pixels
uniform float uSpread; // in [0,1]

noperspective in vec2 vTexcoord;

out vec4 fResult;

const float kTexLevels = 65536.0;
const float kZero = 0.5 * kTexLevels / (kTexLevels - 1.0);
const float kInfinity = 0.0;

/*----------------------------------------------------------------------------*/
vec2 map(vec2 x) {
	return x * (kTexLevels - 1.0) / kTexLevels * 2.0 - 1.0;
}

/*----------------------------------------------------------------------------*/
void main() {		
	vec4 dvec = texture(uDistanceField, vTexcoord);
	dvec.xy = map(dvec.xy);
	float size = float(uSize + 1) / length(vec2(textureSize(uDistanceField, 0)));
	float d = clamp((length(dvec.xy) - size) / (size * uSpread - size), 0.0, 1.0);
	float fac = smoothstep(0.0, 1.0, d);
	fResult = vec4(fac);
}