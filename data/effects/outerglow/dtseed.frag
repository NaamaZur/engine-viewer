/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex; // unpremultiplied argb

out vec4 fResult;

const float kTexLevels = 65536.0;
const float kZero = 0.5 * kTexLevels / (kTexLevels - 1.0);
const float kInfinity = 0.0;

/*----------------------------------------------------------------------------*/
void main() {
	float alpha = texelFetch(uInputTex, ivec2(gl_FragCoord.xy), 0).a;///.r; // alpha in red AE
	
	fResult = vec4(
		vec2(alpha < 0.001 ? kInfinity : kZero),	// background
		vec2(alpha > 0.999 ? kInfinity : kZero)		// foreground
	);
}