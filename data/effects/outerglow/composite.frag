
#version 420 core

layout(binding = 0)uniform sampler2D uGlowTex; // generated glow
layout(binding = 1)uniform sampler2D uSrcTex; // input tex to composite under (unpremultiplied rgba)
uniform float uRange;
uniform vec3 uColor0;
uniform vec3 uColor1;
uniform float uOpacity = 1.0;
uniform float uNoise = 0.0;
uniform float uJitter = 0.0;

noperspective in vec2 vTexcoord;

out vec4 fResult;

/*----------------------------------------------------------------------------*/
float rand(in vec2 seed) {
	return fract(sin(dot(seed.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

/*----------------------------------------------------------------------------*/
void main() {		
	float glow = texture(uGlowTex, vTexcoord).a * uOpacity; ///.r for AE
	vec4 src = texture(uSrcTex, vTexcoord);//.gbar; for AE
	
	float rval = rand(vTexcoord);
	
//	generate glow color
	float d = smoothstep(0.0, 1.0, clamp(glow / (uRange * 2.0), 0.0, 1.0));
	vec3 color = mix(uColor1, uColor0, d * clamp(rval + 1.0 - uJitter, 0.0, 1.0));
	
	if (uColor0 == uColor1){
		glow *= uRange * 0.5 + 0.5;
	}
//	composite under source image
	fResult = vec4(
		color * (1.0 - src.a) + src.rgb * src.a,
		max(src.a, glow * clamp(rval + 1.0 - uNoise, 0.0, 1.0))
	);// AE .argb; // to unpremultiplied argb
}