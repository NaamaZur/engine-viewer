/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uSurfaceTex; // unpremultiplied argb
layout(binding = 1)uniform sampler2D uReflectionTex; // unpremultiplied argb

struct SphereT {
	vec2 center; // in texels
	float radius; // in texels
	vec3 rotation; // Euler angles, in radians
};
uniform SphereT uSphere;

struct LightT {
	vec3 direction;
	vec3 color; // rgb
	int castShadows;
};
uniform LightT uLight;

struct MaterialT {
	float ambient;
	float diffuse;
	float specular;
	float rough;
	float metal;
	float reflective;
	int inside;
	int falloff;
};
uniform MaterialT uMaterial;

out vec4 fResult; // premultiplied rgba

/*----------------------------------------------------------------------------*/
float decel(in float a, in float b, in float delta) {
	float d = 1.0 - delta;
	return mix(a, b, 1.0 - (d * d));
}
/*----------------------------------------------------------------------------*/
float accel(in float a, in float b, in float delta) {
	return mix(a, b, (delta * delta));
}

/*----------------------------------------------------------------------------*/
mat3 rotationFromEuler(in vec3 euler) {
	vec3 cost = vec3(cos(euler.x), cos(euler.y), cos(euler.z));
	vec3 sint = vec3(sin(euler.x), sin(euler.y), sin(euler.z));
	
	mat3 xmat = mat3(
		1.0,		0.0,		0.0,
		0.0,		cost.x,		sint.x,
		0.0,		-sint.x,	cost.x
	);	
	mat3 ymat = mat3(
		cost.y,		0.0,		-sint.y,
		0.0,		1.0,		0.0,
		sint.y,		0.0,		cost.y
	);	
	mat3 zmat = mat3(
		cost.z,		sint.z,		0.0,
		-sint.z,	cost.z,		0.0,
		0.0,		0.0,		1.0
	);
	
	return zmat * ymat * xmat;
}

/*----------------------------------------------------------------------------*/
vec2 latlong(in vec3 v) {
	const float kPi = 3.14159265359;
	const vec2 kInv = vec2(0.1591549, 0.6366198);
	vec2 result;
	result.x = atan(v.x, v.z) + kPi;
	result.y = acos(-v.y) * 0.5;
	return result * kInv;
}

/*----------------------------------------------------------------------------*/
struct SpherePointT {
	vec3 position;
	vec3 normal;
	float radialDistance; // in screen space, from the center
};
void getSpherePoint(in SphereT sphere, in float perspective, in bool inside, out SpherePointT result) {
	vec2 r = gl_FragCoord.xy - sphere.center; // in texels
	result.radialDistance = length(r);
	result.position.xy = r / sphere.radius;
	result.position.z = sqrt((sphere.radius * sphere.radius) - (result.radialDistance * result.radialDistance)) / sphere.radius;
	if (inside) {
		result.position.z = -result.position.z;
		result.position.xy /= perspective;
	} else {
		result.position.xy *= perspective;
	}
	result.position = normalize(result.position);
	result.normal = result.position;
	if (inside) {
		result.normal = -result.normal;
	}
}

/*----------------------------------------------------------------------------*/
void main() {
//	compute weak perspective constant
	const float kPerspectiveMin = 0.2;
	const float kPerspectiveMax = 1.0;
	const float kRadiusMax = 5000.0;
	float pd = 1.0 - (uSphere.radius / kRadiusMax);
	float persp = accel(kPerspectiveMin, kPerspectiveMax, pd);
	persp = clamp(persp, kPerspectiveMin, kPerspectiveMax);
	
//	get point on sphere
	SpherePointT point;
	getSpherePoint(uSphere, persp, bool(uMaterial.inside), point);
	
//	lighting
	float kdiff = clamp(dot(point.normal, uLight.direction), 0.0, 1.0);
	vec3 ref = reflect(uLight.direction, point.normal);
	
	float kspec = 0.0;
	if (kdiff > 0.0) {
		kspec = clamp(dot(ref, vec3(0.0, 0.0, -1.0)), 0.0, 1.0);
		kspec = pow(kspec, (1.0 - uMaterial.rough) * 255.0 + 1.0);
	}
	
	mat3 rmat = rotationFromEuler(uSphere.rotation);//////////
	if (bool(uMaterial.inside) && bool(uLight.castShadows)) {
		float lchord = dot(-point.normal, uLight.direction);
		vec3 stcp = point.position - uLight.direction * lchord * 2.0;		
		vec2 stc = latlong(rmat * stcp);
		float shadow = 1.0 - texture(uSurfaceTex, stc).a; // to rgba
		kdiff *= shadow;
		kspec *= shadow;
	}
	
	kdiff *= uMaterial.diffuse;
	kspec *= uMaterial.specular;
	
	vec4 color = texture(uSurfaceTex, latlong(rmat * point.position));//.gbar; // to rgba
	if (bool(uMaterial.falloff)) {
		color.a *= 1.0 + abs(point.radialDistance / uSphere.radius);
		color.a = clamp(color.a, 0.0, 1.0);
	}
	
	vec3 lresult = uMaterial.ambient * color.rgb * uLight.color; // ambient term
	lresult += kdiff * color.rgb * uLight.color; // diffuse term
	lresult += mix(uLight.color * kspec, uLight.color * color.rgb * kspec, uMaterial.metal); // specular term
	
//	add reflection
	if (uMaterial.reflective > 0.0) {
		vec3 rcolor = texture(uReflectionTex, latlong(point.normal)).rgb * uMaterial.reflective; // to rgba
		lresult += mix(rcolor, rcolor * color.rgb, uMaterial.metal); // reflection term
	}
	
//	generate alpha mask (smooth edge)
	float kSoftness = 2.5;
	float alpha = point.radialDistance <= uSphere.radius ? 1.0 : 0.0;
	alpha *= smoothstep(0.0, 1.0, (uSphere.radius - point.radialDistance - kSoftness) / kSoftness);
	alpha *= color.a;
	
	fResult = vec4(lresult.rgb * alpha , alpha); 
 
	
}