/*******************************************************************************
*******************************************************************************/
#version 420 core

layout(binding = 0)uniform sampler2D uInputTex; // premultiplied rgba

noperspective in vec2 vTexcoord;

out vec4 fResult; // unpremultiplied argb

/*----------------------------------------------------------------------------*/
void main() {
	vec4 result = texture(uInputTex, vTexcoord);
	result.rgb /= result.a; // unpremultiply
	fResult = result;// result.argb; // to argb
}