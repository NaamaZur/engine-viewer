#version 420 core
 
#define IDM_ALPHA        1
#define IDM_LUMA         2
#define IDM_ALPHA_INVRT  3
#define IDM_LUMA_INVRT   4
 
layout(binding=0)  uniform sampler2D COLOR_MAP_0;
layout(binding=1)  uniform sampler2D ALPHA_MAP_0;
 
uniform float uScale = 1.0; // use 1/nSamples
uniform float OPACITY =1.0;  
uniform int ALPHA_ACTIVE = 0;
uniform int unpremult = 0;  // 0 - if the input texture is premult , 1- input texture unpremult //
smooth in vec2 vTexcoord;
 
//flat in int vInstanceId;
 
out vec4 fResult;
 
/*----------------------------------------------------------------------------*/
void main() {
	vec2 flippedUV =  vec2(vTexcoord.s , 1 - vTexcoord.t ) ;
	vec4 result = texture(COLOR_MAP_0,flippedUV);
	/*
	if(unpremult == 1){
		result.rgb/= result.a; ///unpremult (input tex must be premult)
	}
	*/
 //	result.a *= uScale * OPACITY;
	vec4 maskOut = vec4(1);
	 
	switch(ALPHA_ACTIVE){
		  case IDM_ALPHA: // do alpha  mask //
			 maskOut = texture(ALPHA_MAP_0 ,flippedUV);
			 // result.a *= alpha.a;
		  break;
		  case IDM_LUMA:// do luma mask //
			 maskOut = texture(ALPHA_MAP_0 ,flippedUV);
			 float luma = maskOut.r * 0.2126  + maskOut.g * 0.7152 + maskOut.b * 0.0722;
			// result.a *=  luma;
			 maskOut.a = luma;
		  break;
		  case IDM_ALPHA_INVRT:// do inverted alpha mask //
			 maskOut = texture(ALPHA_MAP_0 ,flippedUV);
			 // result.a *=  1 - alpha.a;
			 maskOut.a = 1 - maskOut.a;
		  break;
		  case  IDM_LUMA_INVRT:
			 maskOut = texture(ALPHA_MAP_0 ,flippedUV);
			 float lumainvrt = maskOut.r * 0.2126  + maskOut.g * 0.7152 + maskOut.b * 0.0722;
			// result.a *=  lumainvrt;
			 maskOut.a   =  lumainvrt;
		  break;
	}
 
	 float alpha1 = uScale  * OPACITY * maskOut.a;
	///Now , if the input texture is premultiplied (coming from composition renderer) we do this:
	if(unpremult == 0){
 
		fResult = vec4(result.rgb * alpha1 , result.a * alpha1);/// premult back(output tex must be premult)
 
 
	}else{
		result.a *= alpha1;
		fResult =  vec4(result.rgb * result.a , result.a );
 
	}
}