#version 420 core

//	uniform buffer interface block for instanced rendering (we need to set an
//	upper limit on the array size so that we can access it with gl_InstanceID)
#define MAX_MBLUR_SAMPLES 256
layout(std140, binding=0) uniform TransformationBlock {
	mat4 uModelViewProjection[MAX_MBLUR_SAMPLES];
};

layout(location=0) in vec2 aPosition;

smooth out vec2 vTexcoord;

//flat out int vInstanceId;

/*----------------------------------------------------------------------------*/

out gl_PerVertex
{
	vec4 gl_Position;
};
void main() {
	vTexcoord = aPosition * 0.5 + 0.5;
	gl_Position = uModelViewProjection[gl_InstanceID] * vec4(aPosition, 0.0, 1.0);
	
	//vInstanceId = gl_InstanceID;
}