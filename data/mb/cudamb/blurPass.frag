#version 420 core


layout(binding=0) uniform sampler2D COLOR_MAP_0;
uniform float samples;
uniform float blurScale;
uniform vec3 halfWindowSize;
// ==========  INS   ============//
in vec3 out_velocity;
out vec4 colorOut;
 in vec4 outCol;
void main(void){


            	float w = 1.0 / samples; 
				float wsize =halfWindowSize.x * 2.; 
				float hsize =halfWindowSize.y * 2.; 
				vec4 a=vec4(0.);
				vec2 velocity = out_velocity.xy * blurScale;//  * out_velocity.z * .05;//* .05 * out_velocity.z;

				ivec2 tsize = textureSize(COLOR_MAP_0, 0);
	          	vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));

		 //  	a =  texture(COLOR_MAP_0, vec2(screntc));
			// sample into scene texture along motion vector 
			
				for(int i= 0; i<samples; i++) 
				{
				 
				    float t = i / (samples-1);
				
				//	a =  a + texture(COLOR_MAP_0, outCol.xy + velocity * t ) * w;
				a =  a + textureProj(COLOR_MAP_0, outCol + vec4(velocity.xy,0,0) * t ) * w;
		        //   vec2 offset = velocity * (float(i) / float(samples - 1) - 0.5);
				//	a += texture(COLOR_MAP_0, vec2(screntc)  + offset) ;
					
				}
		
		  	colorOut = a;//a/(samples);

}

/* Check this too:

float4 motionBlur( float2 uv, uniform sampler2D rtTex, uniform sampler2D rtVelocities, int numSteps, float isoFactor, float shutterFactor )
{
    float4 finalColour = 0.0f;
    float2 v = tex2D( rtVelocities, uv ).xy;
    float stepSize = shutterFactor / numSteps;
    for( int i = 0; i < numSteps; ++i )
    {  
        // Sample the color buffer along the velocity vector.  
        finalColour += tex2D( rtTex, uv );
        uv += stepSize;
    }  

    //Clamp the result to prevent saturation when stored in 16 bit float render targets.
    return min( finalColour * isoFactor, 64992.0f );
}

*/