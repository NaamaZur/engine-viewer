#version 420 core

//=========  INS   ==================//

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;


//=========   UNIFORMS   ============//
 uniform mat4 MODEL_VIEW_MATRIX;
 uniform mat4 prevModelView;
 uniform mat4 IDM_PROJ;
 uniform float blurScale;
 uniform vec3 halfWindowSize;

//=======  OUTS  ===================//

  out vec3 out_velocity;
  out vec4 outCol;
out gl_PerVertex
{
    vec4 gl_Position;
};

void main(void){
/*
// transform to eye/camera space:

 	vec4 Pprev =  prevModelView         * position;
 	vec4 P =      MODEL_VIEW_MATRIX     * position;

//get the clip space motion vector:

	mat4 prevModelViewProj = IDM_PROJ *prevModelView;
	P = MODEL_VIEW_MATRIX * position;
	gl_Position =P;// position;//  ///IDM_PROJ * P;

	// calculate window space velocity, do divide by W -> NDC coordinates
	        Pprev = prevModelViewProj * position;
		    P.xyz = P.xyz / P.w;
		    Pprev.xyz = Pprev.xyz / Pprev.w;
		    vec3 velocity =halfWindowSize * (P.xyz - Pprev.xyz);

			float len = length (velocity)/(halfWindowSize[0]*2);
			len = clamp(len, 0., 1.);

			velocity =  normalize(velocity);
			velocity.z = len;
			out_velocity =velocity;

*/

    // transform previous and current position to eye space:
    vec4 P =   position;//   MODEL_VIEW_MATRIX      *
	vec4 Pprev = prevModelView         * position;

	// transform normal to eye space:
	vec3 N = mat3(MODEL_VIEW_MATRIX) * normal;

	// calculate eye space motion vector
	vec3 motionVector = P.xyz - Pprev.xyz;

	// calculate window space motion vector
   P =      position;//    IDM_PROJ *   MODEL_VIEW_MATRIX *
   Pprev =        IDM_PROJ * prevModelView       * position;

	 Pprev = mix( Pprev,P, blurScale);

	 // choose previous or current position based on dot product between motion vector and normal
	
     bool flag = dot(motionVector, N) > 0;
     vec4 Pstretch = flag ? P : Pprev;
     gl_Position =  Pstretch;
	
	 // do divide by W -> NDC coordinates
  P.xyz = P.xyz / P.w;
  Pprev.xyz = Pprev.xyz / Pprev.w;
  Pstretch.xyz = Pstretch.xyz / Pstretch.w;


  // calculate window space velocity
  vec3 dP = (P.xyz - Pprev.xyz) * halfWindowSize.xyz  ;
      /*
  
  	        float len = length (dP)/(halfWindowSize * 2);
			len = clamp(len, 0., 1.);

			dP =  normalize(dP);
			dP.z = len;

		*/
   out_velocity = dP;
 outCol.xy =  0.5 + (dP.xy * 0.005);
// outCol.xy = 0.5 + (dP.xy * 0.005);
}