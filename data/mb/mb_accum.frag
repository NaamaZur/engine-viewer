#version 420 core


// The buffer that we will write to
layout (binding = 0         )  uniform sampler2D COLOR_MAP_0;
layout (binding = 1, rgba32f)  uniform image2D output_buffer;

//==============   INS ======================================//
in smooth vec2 uvsOut;

in flat  vec4 diffuseOut;
 in  vec3 Position;
 in smooth vec3 Normal;

//============== OUTS =======================================//
out vec4 color;

void main(void)
{
	// Load a color from the palette buffer based on primitive ID % 256
 
	///  col*=vec4(1,1,255,1);
	// Store the resulting fragment at two locations. First at the fragments
	// window space coordinate shifted left...


	vec4 col = texture(COLOR_MAP_0, uvsOut).rgba * 255; /// vec4(0,255,0,255);// imageLoad(colors, gl_PrimitiveID & 255);
	imageStore(output_buffer, ivec2(gl_FragCoord.xy) , col);///

	
}
