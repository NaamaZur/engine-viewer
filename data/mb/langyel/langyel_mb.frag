#version 420 core

layout(binding=0           )  uniform sampler2D COLOR_MAP_0;
layout(binding = 0, rgba16f)  uniform image2D VELOCITYMAP;

void main(void){

  vec4 sample;
  vec4 velVec = imageLoad(VELOCITYMAP,ivec2(gl_FragCoord.xy));
  ivec2 tsize = textureSize(COLOR_MAP_0, 0);
  vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));
  vec4 color = texture(COLOR_MAP_0 , screntc);

  //calc the min depth for other color samples:
  float minDepth = velVec.z - max(velVec.w,0.001) * 7.0;

  //init const sample weight:

  sample.w =1;

  //calc coords for first sample on either side:
  vec4 coord = velo.xyxy * vec4(1.75,1.75,-1.75,-1.75) + gl_FragCoord.xyxy;

  //read a color and pdeth at the same location:
  sample.xyz =texture(COLOR_MAP_0,coord.xy).xyz;
  float depth =  imageLoad(VELOCITYMAP,ivec2(coord.xy)).z;

  //add the sample to the final color if it's depth is great enough:
   if(depth >=minDepth)color +=sample;

   //grab the sample on the opposite side of the center pixel:
   sample.xyz = texture(COLOR_MAP_0,coord.zw).xyz;
   depth =  imageLoad(VELOCITYMAP,ivec2(coord.zw)).z;
   if(depth >=minDepth)color+=sample;

   //Calc coordinates for second pair of samples:
   coord = velVec.xyxy * vec4(3.5,3.5,-3.5,-3.5) + gl_FragCoord.xyxy;
   sample.xyz == texture(COLOR_MAP_0,coord.xy).xyz;
   depth =  imageLoad(VELOCITYMAP,ivec2(coord.xy)).z;
   if(depth >=minDepth)color +=sample;

   To Be Continued...


}