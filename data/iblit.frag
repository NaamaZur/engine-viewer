﻿#version 420 core

/*
 * OpenGL Programming Guide - Double Write Example
 */

// Buffer containing the rendered image
///layout (binding = 0, rgba32f) uniform image2DArray output_image;

// This is the output color
out vec4 color;///layout (location = 0) 
uniform int numSamples;

layout(binding=0)uniform sampler2D textureUnit0;
layout(binding=1)uniform sampler2D textureUnit1;
layout(binding=2)uniform sampler2D textureUnit2;
layout(binding=3)uniform sampler2D textureUnit3;
layout(binding=4)uniform sampler2D textureUnit4;
layout(binding=5)uniform sampler2D textureUnit5;


layout(binding=6)uniform sampler2D textureUnit6;
layout(binding=7)uniform sampler2D textureUnit7;
layout(binding=8)uniform sampler2D textureUnit8;
layout(binding=9)uniform sampler2D textureUnit9;
layout(binding=10)uniform sampler2D textureUnit10;
layout(binding=11)uniform sampler2D textureUnit11;
layout(binding=12)uniform sampler2D textureUnit12;
layout(binding=13)uniform sampler2D textureUnit13;
layout(binding=14)uniform sampler2D textureUnit14;
layout(binding=15)uniform sampler2D textureUnit15;

void main(void) 
{ 
      vec2 texelSize = 1.0 / vec2(textureSize(textureUnit0, 0));
  	  vec2 screenTexCoords = gl_FragCoord.xy * texelSize;
	// 0 is the newest image and 5 is the oldest
	vec4 blur0 = texture(textureUnit0, screenTexCoords); 
	vec4 blur1 = texture(textureUnit1, screenTexCoords); 
	vec4 blur2 = texture(textureUnit2, screenTexCoords); 
	vec4 blur3 = texture(textureUnit3, screenTexCoords); 
	vec4 blur4 = texture(textureUnit4, screenTexCoords); 
	vec4 blur5 = texture(textureUnit5, screenTexCoords); 
	
	vec4 blur6 = texture(textureUnit6, screenTexCoords); 
	
	vec4 blur7  = texture(textureUnit7, screenTexCoords); 
	vec4 blur8  = texture(textureUnit8, screenTexCoords); 
	vec4 blur9  = texture(textureUnit9, screenTexCoords); 
	vec4 blur10 = texture(textureUnit10, screenTexCoords); 
	vec4 blur11 = texture(textureUnit11, screenTexCoords); 
	vec4 blur12 = texture(textureUnit12, screenTexCoords); 
	vec4 blur13 = texture(textureUnit13, screenTexCoords); 
	vec4 blur14 = texture(textureUnit14, screenTexCoords); 
	vec4 blur15 = texture(textureUnit15, screenTexCoords); 
	
	
	vec4 summedBlur = blur0	+ blur1 + blur2 + blur3 + blur4 + blur5  + blur6	+ blur7 + blur8 + blur9 + blur10 + blur11+ blur12 + blur13 + blur14 + blur15;

	color = summedBlur / numSamples;
}

/*

void main(void)
{
    int d = 1;
	int layer = 0;
    int actual_layer =  0;///int(max( 0 , 0 )); // max(0, min(d​ - 1, floor(layer​ + 0.5)));
	vec4 col=vec4(0,0,0,0);

	for(int i =0 ; i < numFrames;++i){
	    vec4 samp =vec4(imageLoad(output_image, ivec3(gl_FragCoord.xy,i) ).xyzw) / 255.0;
		
	//	samp.rgb /= samp.a;
	//    vec4 ablend = mix( col, samp, samp.a) ; 
	//	ablend.rgb*=samp.a;
		
    	col += samp;
	
	}
    col /=numFrames;/// vec4(imageLoad(output_image, ivec3(gl_FragCoord.xy,0) ).xyzw) / 255.0;
	color = col;

}

*/
