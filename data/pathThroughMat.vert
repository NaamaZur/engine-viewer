/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 core

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{

	 gl_Position =  position; 
	
}