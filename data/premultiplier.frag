/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Texture pass through Material Fragment Shader.
*/

#version 420 

layout(binding=0) uniform sampler2D  COLOR_MAP_0;

uniform int premult = 0;  /// 0 - unpremultiply , 1 - premuliply 
uniform ivec2 blitOffset;
uniform ivec2 finalTexSize;
noperspective in vec2 vTexcoord; ///
out vec4 OUTPUT;
 

void main(void) {
	 
		ivec2 tsize =  textureSize(COLOR_MAP_0, 0);
		vec2 screntc = gl_FragCoord.xy * (1.0 / vec2(tsize));
 
	 
	   vec2 toff =   vec2(1.0 / vec2(tsize) * blitOffset);
	   vec2 texSizeInTexels = vec2(1.0 / vec2(tsize) * tsize);
	 
		 vec4 tex;
		if(premult == 0){
			 tex = texture(COLOR_MAP_0,vec2( screntc.x, 1.0 - screntc.y));
			OUTPUT =     vec4(tex.rgb / tex.a,tex.a)  ;
		 
		}else if(premult == 1){
		     tex = texture(COLOR_MAP_0,vec2( screntc.x, 1.0 - screntc.y));
			 OUTPUT =     vec4(tex.rgb * tex.a,tex.a);

		}else{
			
		   

				OUTPUT =  texture(COLOR_MAP_0,vec2(  screntc.x - toff.x, ( 1 - screntc.y)- toff.y)); 
				  if(
				  ( screntc.x < toff.x || screntc.x > (toff.x + texSizeInTexels.x) )||
				  ( screntc.y < toff.y || screntc.y > (toff.y + texSizeInTexels.y) )
				   )
				   {
				 
				    OUTPUT.a = 0;

			      }


		}

	 

}