/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 

 in  flat vec4 diffuseOut;


//===== out =======
out vec4 OUTPUT;

  void main(void){

    OUTPUT =  diffuseOut;

 }