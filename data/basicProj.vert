/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 core

layout(location = 0)  in vec4 position;
layout(location = 1)  in vec2 uvs;
layout(location = 2)  in vec3 normal;

uniform mat4 MVP;
smooth out vec2 vTexcoord;
out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
     vTexcoord = uvs;
	 gl_Position = MVP *  position; 
	
}