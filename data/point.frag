/*
Author     : Michael Ivanov, IDOMOO.INC
Description:Color Material Vertex Shader.
*/
#version 420 
//===================  Subroutines ==============================
subroutine vec4 RenderMode();
subroutine uniform RenderMode renderMode;

//====================  Uniforms  ===============================
// layout(binding=0)  uniform sampler2D COLOR_MAP_0;

//===================   Ins      ================================
 


//===================  Out     =================================
out vec4 OUTPUT;




subroutine (RenderMode) vec4 proceduralPoint(){
    const vec4 col1 =vec4(0.6,0,0,1);
	const vec4 col2 =vec4(0.9,0.9,1,1);

	vec2 temp =gl_PointCoord - vec2(0.5);
	float f = dot(temp , temp);
	if(f > 0.55){
	   discard;
	}
  return mix(col1 , col2 , smoothstep(0.1,0.35,f)); 
}

subroutine (RenderMode) vec4 texturePoint(){
///not used for now
 return vec4(1,0,0,1);// texture(COLOR_MAP_0 ,gl_PointCoord);

}


 void main(void){

 
	OUTPUT = renderMode();


}