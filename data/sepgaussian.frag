#version 420 core
//////// add subroutines later : ///////////////
layout(binding=0) uniform sampler2D COLOR_MAP_0;

uniform float MAX_BLUR_RADIUS; 

uniform int BLUR_TYPE;
smooth in float BLUR_RADIUS; // from the vertex shader

layout(location=0) out vec4 OUTPUT;
// **********************     Vertical Blur ************************** //
vec4 gaussBlurX(in vec2 origin, in vec2 texelSize) {

	int nSamples = clamp(int(BLUR_RADIUS), 1, int(MAX_BLUR_RADIUS));

           if (nSamples == 1){
               return texture(COLOR_MAP_0, origin);

           }
		
	const float SIGMA =  max(float(nSamples) / 8.0, 0.5);
    float sig2 = SIGMA * SIGMA;
	const float TWO_PI = 6.28318531;
	const float E = 2.71828182;

	vec4 result = vec4(0.0);	
	float weightSum = 0.0;
    vec2 dir=vec2(1,0);
	for (int i = 0; i <  nSamples ; ++i) {
	

		 float x =float(i) - float(nSamples / 2);////float(i) / nSamples;/////// float y = float(i) - float(nSamples / 2);
		 float exponent = (x * x) / (2.0 * sig2);
		 float weight = (1.0 / sqrt(TWO_PI * sig2)) * pow(E, -exponent);
	
		result += texture(COLOR_MAP_0, origin + x * texelSize * dir) * weight;
	}
	
	return result ;
    
///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// demo////////////////////////////////
    /*    int nSamples = clamp(int(BLUR_RADIUS), 1, int(MAX_BLUR_RADIUS));	
	const float SIGMA =  max(float(nSamples) / 4.0, 0.5);
	const float TWO_PI = 6.28318531;
	const float E = 2.71828182;

	vec4 result = vec4(0.0);	
	float weightSum = 0.0;
	for (int i = 0; i < nSamples * nSamples; ++i) {
	//	calculate offset:
		ivec2 coord = ivec2(	
			i / nSamples, // calculate x
			i - (i / nSamples) * nSamples // calculate y
		);
		vec2 offset = vec2(coord) - vec2(float(nSamples / 2));
		
	//	calculate weight:
		float sig2 = SIGMA * SIGMA;
		float exponent = (offset.x * offset.x + offset.y * offset.y) / (2.0 * sig2);
		float weight = pow(1.0 / (TWO_PI * sig2) * E, exponent);
		weightSum += weight;
				
	//	transform offset to texture space:
		offset *= texelSize;
		
	//	accumulate result:
		result += texture(TEXTURE, origin + offset) * weight;
	}
	
	return result ;
*/
///////////////////////////////////////////////////////////////////////////////
}
// **********************     Horizontal Blur ************************** //
vec4 gaussBlurY(in vec2 origin, in vec2 texelSize) {
//	calculate n samples to use:
	int nSamples = clamp(int(BLUR_RADIUS), 1, int(MAX_BLUR_RADIUS));

           if (nSamples == 1){
               return texture(COLOR_MAP_0, origin);

           }
		
	const float SIGMA =  max(float(nSamples) / 8.0, 0.5);
    float sig2 = SIGMA * SIGMA;
	const float TWO_PI = 6.28318531;
	const float E = 2.71828182;

	vec4 result = vec4(0.0);	
	float weightSum = 0.0;
    vec2 dir=vec2(0,1);
	for (int i = 0; i <= nSamples ; ++i) {
	

		 float y = float(i) - float(nSamples / 2);
		 float exponent = (y * y) / (2.0 * sig2);
		 float weight = (1.0 / sqrt(TWO_PI * sig2)) * pow(E, -exponent);
	
		result += texture(COLOR_MAP_0, origin + y * texelSize * dir) * weight;
	}
	
	return result ;
}
void main(void) {
	vec2 texelSize = 1.0 / vec2(textureSize(COLOR_MAP_0, 0));
	vec2 screenTexCoords = gl_FragCoord.xy * texelSize;
	//OUTPUT = texture(COLOR_MAP_0,screenTexCoords);
	
         if(BLUR_TYPE == 0){
             OUTPUT = gaussBlurX(screenTexCoords, texelSize);
         }else{
             OUTPUT = gaussBlurY(screenTexCoords, texelSize);
         }
	
			
	
}