
#version 420 compatibility
#extension GL_NV_bindless_texture : require
#extension GL_NV_gpu_shader5 : require // for uint64_t

#define NUM_TEXTURES  100  ///max num of textures

layout(binding=1)uniform SamplersNV ///std140
{ 

  sampler2D allTheSamplers[NUM_TEXTURES];

};
 
smooth in vec2 vTexcoord;
flat  in int vInstanceId;
out vec4 OUTPUT;

void main (void){

   
   sampler2D currSampler = allTheSamplers[vInstanceId];
  

  

	  OUTPUT = texture(currSampler, vTexcoord );
 
}




