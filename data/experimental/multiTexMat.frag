#version 420 



layout(binding=0)uniform sampler2D Diffuse1;
layout(binding=1)uniform sampler2D Diffuse2;
layout(binding=2)uniform sampler2D Diffuse3;
layout(binding=3)uniform sampler2D Diffuse4;
in smooth  vec2 vUVs;

out vec4 OUTPUT;

void main (void){


   if(vUVs.x < 0.5 && vUVs.y < 0.5){

	  OUTPUT = texture(Diffuse1 , vUVs );

   }else if(vUVs.x > 0.5 && vUVs.y < 0.5){

	  OUTPUT = texture(Diffuse2 , vUVs );

   }else if(vUVs.x < 0.5 && vUVs.y  > 0.5){

	  OUTPUT = texture(Diffuse3 , vUVs );

  }else{

	  OUTPUT = texture(Diffuse4 , vUVs );

  }

	


}


