#ifndef UIWIDGET_H
#define UIWIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}
class UIWidget : public QWidget
{
    Q_OBJECT
public:
    explicit UIWidget(QWidget *parent = 0);
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    Ui::Widget* GetUI(){
        return ui;
    }

private:
      Ui::Widget *ui;

signals:

public slots:



};

#endif // UIWIDGET_H
