#-------------------------------------------------
#
# Project created by QtCreator 2013-10-07T12:06:28
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ENGINE_GUI
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    mainwindow.cpp \
    uiwidget.cpp \
    enginecontainer.cpp

HEADERS  += enginecontainer.h\
   mainwindow.h \
     widget.h \
    uiwidget.h



FORMS    += widget.ui
#--------------------  GLEW ------------------------------
#win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/glew-1.10.0/lib/-lglew32

#INCLUDEPATH += $$PWD/../Documents/OpenGL_Stuff/glew-1.10.0/include
#DEPENDPATH += $$PWD/../Documents/OpenGL_Stuff/glew-1.10.0/include
#win32: PRE_TARGETDEPS += $$PWD"/../Documents/OpenGL_Stuff/glew-1.10.0/lib/glew32.lib"


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../Documents/OpenGL_Stuff/glew-1.10.0/lib/Release/x64/ -lglew32
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../Documents/OpenGL_Stuff/glew-1.10.0/lib/Release/x64/ -lglew32

INCLUDEPATH += $$PWD/../Documents/OpenGL_Stuff/glew-1.10.0/include
DEPENDPATH += $$PWD/../Documents/OpenGL_Stuff/glew-1.10.0/include

#------------   GLM  ------------------------------
INCLUDEPATH += $$PWD/../Documents/OpenGL_Stuff/glm-0.9.4.1/glm
DEPENDPATH  += $$PWD/../Documents/OpenGL_Stuff/glm-0.9.4.1/glm

#--------------------  IDMOOO ENGINE ------------------------------
#win32: LIBS += -L$$PWD/ -lIDOMOO_ENGINE
#win32:CONFIG(release, debug|release):  LIBS += -L$$PWD"/../Documents/visual studio 2012/Projects/IDOMOO_ENGINE/x64/Debug/ -lIDOMOO_ENGINE"

#INCLUDEPATH += $$PWD/../Documents/engine_headers/include
#DEPENDPATH += $$PWD/../Documents/engine_headers/include
#INCLUDEPATH += $$PWD"/../Documents/visual studio 2012/Projects/IDOMOO_ENGINE/IDOMOO_ENGINE/include"
#DEPENDPATH += $$PWD"/../Documents/visual studio 2012/Projects/IDOMOO_ENGINE/IDOMOO_ENGINE/include"
#win32: PRE_TARGETDEPS += $$PWD"/../Documents/visual studio 2012/Projects/IDOMOO_ENGINE/x64/Debug/IDOMOO_ENGINE.lib"
#win32: PRE_TARGETDEPS += $$PWD/IDOMOO_ENGINE.lib

win32: LIBS +=  -L$$PWD/ -lIDOMOO_ENGINE

INCLUDEPATH += $$PWD"/../Documents/visual studio 2012/Projects/IDOMOO_ENGINE/IDOMOO_ENGINE/include"
DEPENDPATH += $$PWD"/../Documents/visual studio 2012/Projects/IDOMOO_ENGINE/IDOMOO_ENGINE/include"

win32: PRE_TARGETDEPS +=  $$PWD/IDOMOO_ENGINE.lib





#------------------------------  DEVIL  --------------------------------------------
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/DevIL-SDK-x64-1.7.8/ -lDevIL
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/DevIL-SDK-x64-1.7.8/ -lILU
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/DevIL-SDK-x64-1.7.8/ -lILUT
INCLUDEPATH += $$PWD/../Documents/OpenGL_Stuff/DevIL-SDK-x64-1.7.8/include
DEPENDPATH += $$PWD/../Documents/OpenGL_Stuff/DevIL-SDK-x64-1.7.8/include

#-------------------------------  BOOST  ------------------------------------------------


#win32: LIBS += -L$$PWD/../boost/boost_1_50/lib/ -lboost_date_time-vc100-mt-1_50
#win32: LIBS += -L$$PWD/../boost/boost_1_50/lib/ -lboost_thread-vc100-mt-1_50
win32: LIBS += -LC:/boost_1_54_0VC11_64bit/stage/lib/ -lboost_date_time-vc110-mt-gd-1_54
win32: LIBS += -LC:/boost_1_54_0VC11_64bit/stage/lib/ -lboost_thread-vc110-mt-gd-1_54

#INCLUDEPATH += $$PWD/../boost/boost_1_50
#DEPENDPATH  += $$PWD/../boost/boost_1_50
INCLUDEPATH += C:/boost_1_54_0VC11_64bit
DEPENDPATH  += C:/boost_1_54_0VC11_64bit

#------------------------------------  FREEIMAGE  ------------------------------------------

win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/FreeImage/Dist/ -lFreeImage

INCLUDEPATH += $$PWD/../Documents/OpenGL_Stuff/FreeImage/Dist
DEPENDPATH += $$PWD/../Documents/OpenGL_Stuff/FreeImage/Dist

#------------------------------------- GLFW --------------------------------------------------
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/glfw-3.0.4.bin.WIN64/lib-msvc110/ -lglfw3

INCLUDEPATH += $$PWD/../Documents/OpenGL_Stuff/glfw-3.0.4.bin.WIN64/include
DEPENDPATH += $$PWD/../Documents/OpenGL_Stuff/glfw-3.0.4.bin.WIN64/include

#------------------------------------  FFMPEG (if needed ) ------------------------------------

win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/lib/ -lavcodec
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/lib/ -lavdevice
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/lib/ -lavfilter
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/lib/ -lavformat
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/lib/ -lavutil
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/lib/ -lswscale
win32: LIBS += -L$$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/lib/ -lswresample
INCLUDEPATH += $$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/include
DEPENDPATH += $$PWD/../Documents/OpenGL_Stuff/ffmpeg-20130418/include

OTHER_FILES += \
    PO2_demo1.png



