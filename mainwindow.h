#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "widget.h"
#include <QWidget>

QT_BEGIN_NAMESPACE
class QSlider;
QT_END_NAMESPACE

#include "uiwidget.h"
class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

    void keyPressEvent(QKeyEvent *e);
private:
    QSlider *createSlider();
    QSlider *xSlider;
    QSlider *ySlider;
    QSlider *zSlider;
    GLWidget *glWidget;
UIWidget * uiw;
signals:

public slots:

};

#endif // MAINWINDOW_H
